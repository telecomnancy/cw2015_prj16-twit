Limoncello

(c) 2015, Malo Boixel
(c) 2015, Dorian Geraut
(c) 2015, Martin Horn
(c) 2015, Gabrielle Toulet Morlannne

License: GPL v2

1.Description
 Limoncello is an application that collects and analyzes tweets from the Twitter website. As far as the collect, the analyze of an amount of tweets can be selected depending on various criteras. Limoncello offers graphics representations of a collect on the user's criteras and the possibility to export each collect.

2. Used Libraries
 The libraries used in this project are :
  Sqlite-jdbc (Apache License 2.0)
  Twitter4j (Apache License 2.0.)
  JFreeChart (GNU Lesser General Public Licence (LGPL))
  OpenStreetMap.MapViewer (GPL v2)

2.Description
 //descriptions of all the project, and all sub-modules and libraries

3. How to run Limoncello
 Place yourself in directory :
	cd /path_to_project/Release/
 To run the application, use the following commands, depending on the mode (console or graphical interface)
	java -jar codingWeek.jar -console
	java -jar codingWeek.jar -ig

4. License 
 Read License.txt   

5. Documentation
 Read Documentation.pdf

6. Releases and features
 The latest code could be find on : https://bitbucket.org/telecomnancy/cw2015_prj16-twit. To submit bug, feature requests or
 join the developpers community, please contact our Twitter account : @LimoncelloCW.

