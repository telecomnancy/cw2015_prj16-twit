/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import API.MainAPI;
import App.Controler;
import App.Main;
import Data.Data;
import Data.ModelParameter;
import Data.Tweet;
import JDBC.SQLiteJDBC;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author boixel
 */
public class BrowseTest {
    private boolean ok;
    
    public BrowseTest() {
        boolean isStream = false;
        ModelParameter parameter = ModelParameter.getInstance();
                
                Data modele = Data.getInstance();
                Controler controler = new Controler();
                
		for (int i = 0; i < 12; i++) {
			ModelParameter.getInstance().setFieldKept(true, i, true);
		}
		for (int i = 0; i < 5; i++) {
			ModelParameter.getInstance().setFieldKept(false, i, true);
		}
                ModelParameter.getInstance().setFilterField(true, Main.TEXT,
				"codingweek");
		ModelParameter.getInstance().setFilterField(true, Main.LANG, "fr");
                ModelParameter.getInstance().setNB_TWEETS(100);
                ModelParameter.getInstance().setVisibleConsole(false);
         System.out.println("On effectue une recherche dans le passé sur le mot codingweek");
		new SQLiteJDBC().store(new MainAPI(isStream).extractData());
                System.out.println("On teste si les tweets collectés contiennent effectivement le mot codingweek");
                List<Tweet> tweets = new ArrayList<Tweet>();
                tweets = modele.getTweets();
                
                
                String texte = "(?i)codingweek";
                Pattern p = Pattern.compile(texte);
                for(Tweet tweet : tweets) {
                    Matcher m = p.matcher(tweet.getBody());
                    ok = m.find();
                    System.out.println("Le tweet contient le mot codingweek :\n");
                    System.out.println(ok);
                    if(!ok)
                        System.out.println(tweet.getBody());
                    hello();
                }
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void hello() {
         if(!ok)
             fail("Le tweet ne contient pas le mot recherché");
     }
}
