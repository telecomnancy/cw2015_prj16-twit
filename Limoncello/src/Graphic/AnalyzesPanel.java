package Graphic;

import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Analyze.Analyze;
import Analyze.HistogramAnalyze;
import Analyze.MapAnalyze;
import App.Controler;
import GraphicAnalyze.HistogramPanel;
import GraphicAnalyze.MapPanel;

public class AnalyzesPanel extends JTabbedPane {
	private Analyze analyze;

	public AnalyzesPanel(Controler controler) {
		super();
		this.add("Resultats", new HistogramPanel(controler));
		this.add("Carte", new MapPanel(controler));
		this.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {

				if (getSelectedComponent() instanceof HistogramPanel) {
					analyze = new HistogramAnalyze();
				} else if (getSelectedComponent() instanceof MapPanel) {
					analyze = new MapAnalyze();
				} else {

				}
				controler.switchAnalyze(analyze, getSelectedComponent());
			}
		});
	}

}
