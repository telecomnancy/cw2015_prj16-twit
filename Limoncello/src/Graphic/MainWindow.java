package Graphic;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import App.Controler;

public class MainWindow {
	private MenuPanel menupanel;
	private JTabbedPane Tab;
	private JPanel All;
	private JPanel collectPanel;
	private JPanel authorPanel;
	private JTabbedPane analyzesPane;
	private Controler controler;

	public MainWindow(Controler controler) {
		this.controler = controler;
		// Theme of OS look
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// End of theme change

		JFrame fenetre = new JFrame("Limoncello");
		fenetre.setSize(1900, 1000);

		All = new JPanel(new BorderLayout());
		Tab = new JTabbedPane();

		JPanel gauche = new JPanel(new BorderLayout());
		collectPanel = new CollectPanel(controler);
		authorPanel = new AuthorPanel(controler);
		analyzesPane = new AnalyzesPanel(controler);
		menupanel = new MenuPanel(controler);

		gauche.add(collectPanel, BorderLayout.NORTH);
		gauche.add(authorPanel, BorderLayout.CENTER);
		Tab.add("Collecte de tweets", gauche);
		Tab.add("Analyse de collecte", analyzesPane);

		All.add(Tab, BorderLayout.CENTER);
		All.add(menupanel, BorderLayout.NORTH);
		fenetre.add(All);
		fenetre.setVisible(true);
	}
}
