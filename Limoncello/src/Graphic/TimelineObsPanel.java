package Graphic;

import java.awt.BorderLayout;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import App.Controler;
import Data.Data;
import Data.ModelParameter;
import Data.Tweet;

public class TimelineObsPanel extends JPanel implements Observer {
	private JTextArea textArea;

	public TimelineObsPanel(Controler controler) {
		super(new BorderLayout());

		ModelParameter.getInstance().addObserver(this);

		JTextArea ta = new JTextArea();
		this.textArea = ta;
		textArea.setFocusable(false);
		JScrollPane jp = new JScrollPane(textArea);
		jp.setBorder(BorderFactory.createTitledBorder("Timeline de l'auteur"));
		this.add(jp);
	}

	public void write(Tweet tweet) {
		textArea.append(tweet.toString());
	}

	public void update(Observable obs, Object obj) {
		long l = ModelParameter.getInstance().getObservedAuthorId();
		List<Tweet> tweets = Data.getInstance().getAuthorTimeline(l);
		if (tweets != null)
			for (Tweet t : tweets)
				write(t);

	}
}
