package Graphic;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import App.Controler;
import App.Main;

public class FiltrePanel extends JPanel {

	private JButton CollectButton;
	private JPanel TweetPanel = new JPanel();
	private JPanel AuthorPanel = new JPanel();
	private JPanel TopPanel = new JPanel(new GridLayout(1, 2));
	private FiltreUnitPanel geoloc;
	private FiltreUnitPanel language;
	private FiltreUnitPanel date;
	private FiltreUnitPanel text;
	private FiltreUnitPanel hashtag;
	private FiltreUnitPanel image;
	private FiltreUnitPanel ModeStream;
	private FiltreUnitPanel ModeBrows;
	private FiltreUnitPanel retweet;
	private FiltreUnitPanel retweetFav;
	private FiltreUnitPanel reetwit_Auth_id;
	private FiltreUnitPanel in_reply;
	private FiltreUnitPanel author;
	private FiltreUnitPanel name;
	private FiltreUnitPanel followers;
	private FiltreUnitPanel follow;
	private FiltreUnitPanel languageAuthor;
	private FiltreUnitPanel analyse;

	// A little mehode to test if a String can be cast in an integer and return
	// a boolean
	public static boolean isNumeric(String str) {
		try {
			int test = Integer.parseInt(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	public FiltrePanel(Controler controler) {
		super(new BorderLayout());

		CollectButton = new JButton("Collecter");

		CollectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				controler.extractAndStore();
				// SaveDBM.setEnabled(true);
				// SaveCSVM.setEnabled(true);
				CollectButton.setEnabled(false);
			}
		});

		JPanel tp = new JPanel(new GridLayout(13, 1));
		JPanel ap = new JPanel(new GridLayout(4, 1));

		JCheckBox geolocCheck = new JCheckBox("geolocalisation", true);
		JCheckBox languageCheck = new JCheckBox("langue", true);
		JCheckBox dateCheck = new JCheckBox("date", true);
		JCheckBox textCheck = new JCheckBox("texte", true);
		JCheckBox hashtagCheck = new JCheckBox("hashtag", true);
		JCheckBox imageCheck = new JCheckBox("image", false);
		JCheckBox retweetCheck = new JCheckBox("nombre de fois partagé", true);
		JCheckBox retweetFavCheck = new JCheckBox("nombre de fois favori", true);
		JCheckBox authorCheck = new JCheckBox("auteur", true);
		JCheckBox nameCheck = new JCheckBox("nom", true);
		JCheckBox followersCheck = new JCheckBox("nombre de followers", true);
		JCheckBox followCheck = new JCheckBox("<HTML>nombre de personnes suivies par l'auteur</HTML>", true);

		JCheckBox reetwit_Auth_id1 = new JCheckBox("Auteur original si RT", true);
		JCheckBox in_reply1 = new JCheckBox("Tweet original si réponse", true);

		JCheckBox languageAuthorCheckBox = new JCheckBox("langue", true);
		JCheckBox ModeStreamCheck = new JCheckBox("<HTML> Mode Stream <br> (temps en minutes)<HTML>", true);
		JCheckBox ModeBrowsCheck = new JCheckBox("<HTML> Mode Browse <br> (nb de tweets collectés)<HTML>", false);

		JTextField languageText = new JTextField("fr", 10);
		JTextField textText = new JTextField("codingweek", 10);
		JTextField hashtagText = new JTextField(10);
		JTextField ModeStreamText = new JTextField("1", 10);

		JTextField ModeBrowsText = new JTextField("100", 10);
		ModeBrowsText.setEnabled(false);

		JLabel labelVide = new JLabel("");
		this.geoloc = new FiltreUnitPanel(geolocCheck, null);
		geoloc.add(labelVide, BorderLayout.EAST);
		this.language = new FiltreUnitPanel(languageCheck, languageText);
		this.date = new FiltreUnitPanel(dateCheck, null);
		date.add(labelVide, BorderLayout.EAST);
		this.text = new FiltreUnitPanel(textCheck, textText);
		this.hashtag = new FiltreUnitPanel(hashtagCheck, hashtagText);
		this.image = new FiltreUnitPanel(imageCheck, null);
		this.image.add(labelVide, BorderLayout.EAST);

		this.ModeStream = new FiltreUnitPanel(ModeStreamCheck, ModeStreamText);
		this.ModeBrows = new FiltreUnitPanel(ModeBrowsCheck, ModeBrowsText);

		this.author = new FiltreUnitPanel(authorCheck, null);
		this.author.add(labelVide, BorderLayout.EAST);

		this.name = new FiltreUnitPanel(nameCheck, null);
		this.followers = new FiltreUnitPanel(followersCheck, null);
		this.follow = new FiltreUnitPanel(followCheck, null);

		this.retweet = new FiltreUnitPanel(retweetCheck, null);
		retweet.add(labelVide, BorderLayout.EAST);
		this.retweetFav = new FiltreUnitPanel(retweetFavCheck, null);
		retweetFav.add(labelVide, BorderLayout.EAST);
		this.reetwit_Auth_id = new FiltreUnitPanel(reetwit_Auth_id1, null);
		this.reetwit_Auth_id.add(labelVide, BorderLayout.EAST);
		this.in_reply = new FiltreUnitPanel(in_reply1, null);
		this.in_reply.add(labelVide, BorderLayout.EAST);
		this.languageAuthor = new FiltreUnitPanel(languageAuthorCheckBox, null);

		// Add all the actions for the CheckBoxs

		geoloc.getCheckBox().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Controler.setFieldKept(true, Main.GEOLOC, geoloc.getCheckBox().isSelected());
			}
		});

		language.getCheckBox().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Controler.setFieldKept(true, Main.LANG, language.getCheckBox().isSelected());

			}
		});

		date.getCheckBox().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Controler.setFieldKept(true, Main.DATE, date.getCheckBox().isSelected());

			}
		});

		text.getCheckBox().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Controler.setFieldKept(true, Main.TEXT, text.getCheckBox().isSelected());

			}
		});

		hashtag.getCheckBox().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Controler.setFieldKept(true, Main.HASH, hashtag.getCheckBox().isSelected());

			}
		});

		image.getCheckBox().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Controler.setFieldKept(true, Main.IMG, image.getCheckBox().isSelected());

			}
		});

		ModeStream.getCheckBox().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Controler.setBackToTheFuture(true);
				ModeStream.getTextField().setEnabled(ModeStream.getCheckBox().isSelected());
				ModeBrows.getTextField().setEnabled(ModeBrows.getCheckBox().isSelected() == false);
				ModeBrows.getCheckBox().setSelected(ModeBrows.getCheckBox().isSelected() == false);

			}
		});

		ModeBrows.getCheckBox().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Controler.setBackToTheFuture(false);
				ModeBrows.getTextField().setEnabled(ModeBrows.getCheckBox().isSelected());
				ModeStream.getTextField().setEnabled(ModeBrows.getCheckBox().isSelected() == false);
				ModeStream.getCheckBox().setSelected(ModeBrows.getCheckBox().isSelected() == false);
			}
		});

		retweet.getCheckBox().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				Controler.setFieldKept(true, Main.RT_COUNT, retweet.getCheckBox().isSelected());

			}
		});

		retweetFav.getCheckBox().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				Controler.setFieldKept(true, Main.FAV_COUNT, retweet.getCheckBox().isSelected());

			}
		});

		author.getCheckBox().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Controler.setFieldKept(true, Main.AUTHOR, author.getCheckBox().isSelected());
				AuthorPanel.setVisible(author.getCheckBox().isSelected()); // Pour
				// afficher
				// ou
				// non
				// le
				// parametrage
				// de
				// la
				// partie
				// auteur
			}
		});

		name.getCheckBox().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (author.getCheckBox().isSelected()) {
					Controler.setFieldKept(false, Main.NAME, name.getCheckBox().isSelected());

				}
			}

		});

		followers.getCheckBox().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (author.getCheckBox().isSelected()) {
					Controler.setFieldKept(false, Main.FOLLOWERS_CNT, followers.getCheckBox().isSelected());
				}
			}
		});

		follow.getCheckBox().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (author.getCheckBox().isSelected()) {
					Controler.setFieldKept(false, Main.FOLLOW_CNT, follow.getCheckBox().isSelected());
				}
			}
		});
		/*
		 * Standing By reetwit_Auth_id1.addActionListener(new ActionListener() {
		 * 
		 * @Override public void actionPerformed(ActionEvent e) { if
		 * (reetwit_Auth_id2.isSelected()) { Controler.setFieldKept(false,
		 * Main.RT_ID, follow .getCheckBox().isSelected()); } } });
		 * 
		 * reetwit_Auth_id2.addActionListener(new ActionListener() {
		 * 
		 * @Override public void actionPerformed(ActionEvent e) { if
		 * (reetwit_Auth_id2.isSelected()) { Controler.setFilterField(false,
		 * Main.RT_ID, "true"); } else { Controler.setFilterField(false,
		 * Main.RT_ID, "false"); } } });
		 * 
		 * in_reply1.addActionListener(new ActionListener() {
		 * 
		 * @Override public void actionPerformed(ActionEvent e) { if
		 * (reetwit_Auth_id2.isSelected()) { Controler.setFieldKept(false,
		 * Main.REPLY_ID, follow .getCheckBox().isSelected()); } } });
		 * 
		 * in_reply2.addActionListener(new ActionListener() {
		 * 
		 * @Override public void actionPerformed(ActionEvent e) { if
		 * (in_reply2.isSelected()) { Controler.setFilterField(false,
		 * Main.REPLY_ID, "true"); } else { Controler.setFilterField(false,
		 * Main.REPLY_ID, "false"); } } });
		 */

		// Add all the actions for the TextBoxs

		language.getTextField().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Controler.setFilterField(true, Main.LANG, language.getTextField().getText());
			}
		});

		text.getTextField().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Controler.setFilterField(true, Main.TEXT, text.getTextField().getText());
			}
		});

		hashtag.getTextField().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Controler.setFilterField(true, Main.HASH, hashtag.getTextField().getText());

			}
		});

		ModeStream.getTextField().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (FiltrePanel.isNumeric(ModeStream.getTextField().getText())) {
					try {
						Controler.setTimer(Integer.parseInt(ModeStream.getTextField().getText()));
					} catch (Exception exception) {
						ModeStream.getTextField().setText("il faut rentrer un entier");
					}
				}

			}
		});

		ModeBrows.getTextField().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (FiltrePanel.isNumeric(ModeBrows.getTextField().getText())) {
					try {
						Controler.setNB_TWEETS(Integer.parseInt(ModeBrows.getTextField().getText()));
					} catch (Exception exception) {
						ModeStream.getTextField().setText("il faut rentrer un entier");
					}
				}

			}
		});
		/*
		 * Standin by retweet.getTextField().addActionListener(new
		 * ActionListener() {
		 * 
		 * @Override public void actionPerformed(ActionEvent e) {
		 * 
		 * if (FiltrePanel.isNumeric(retweet.getTextField().getText())) {
		 * Controler.setFilterField(true, Main.REPLY_COUNT, retweet
		 * .getTextField().getText()); }
		 * 
		 * } });
		 * 
		 * retweetFav.getTextField().addActionListener(new ActionListener() {
		 * 
		 * @Override public void actionPerformed(ActionEvent e) { if
		 * (FiltrePanel.isNumeric(retweetFav.getCheckBox().getText())) {
		 * Controler.setFilterField(true, Main.FAV_COUNT, retweet
		 * .getTextField().getText()); } } });
		 */

		tp.add(geoloc);
		tp.add(language);
		tp.add(date);
		tp.add(text);
		tp.add(hashtag);
		tp.add(image);
		tp.add(retweet);
		tp.add(retweetFav);
		tp.add(reetwit_Auth_id);
		tp.add(in_reply);
		tp.add(author);
		tp.add(ModeStream);
		tp.add(ModeBrows);
		ap.add(name);
		ap.add(followers);
		ap.add(follow);
		ap.add(languageAuthor);
		tp.setBorder(BorderFactory.createTitledBorder("Information récoltée et filtre sur tweet:"));
		ap.setBorder(BorderFactory.createTitledBorder("Information récoltée sur auteur:"));

		TweetPanel = tp;
		AuthorPanel = ap;

		TopPanel.add(TweetPanel, BorderLayout.CENTER);
		TopPanel.add(AuthorPanel, BorderLayout.EAST);

		this.add(TopPanel, BorderLayout.CENTER);
		this.add(CollectButton, BorderLayout.SOUTH);
	}
}
