package Graphic;

import java.awt.GridLayout;

import javax.swing.JPanel;

import App.Controler;

public class CollectPanel extends JPanel {
	private FiltrePanel fp;
	private TweetObsPanel top;

	public CollectPanel(Controler controler) {
		super(new GridLayout(1, 2));
		fp = new FiltrePanel(controler);
		top = new TweetObsPanel(controler);
		this.add(fp);
		this.add(top);
	}

}
