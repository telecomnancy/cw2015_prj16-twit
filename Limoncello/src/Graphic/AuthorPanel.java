package Graphic;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import App.Controler;

public class AuthorPanel extends JPanel {
	private JLabel info;
	private JTextField authorID;
	private JPanel authorBox;
	private TimelineObsPanel top;

	public AuthorPanel(final Controler controler) {
		super(new GridLayout(1, 2));

		info = new JLabel("Plus d'informations sur l'auteur :");
		authorID = new JTextField("Auteur ID");
		authorID.setPreferredSize(new Dimension(200, 30));
		authorID.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Long l = Long.parseLong(authorID.getText(), 10);
					controler.setObservedAuthorId(l);
				} catch (NumberFormatException exception) {
					authorID.setText("Vous devez rentrer un id d'auteur");
				}
			}
		});

		authorBox = new JPanel();
		authorBox.add(info);
		authorBox.add(authorID);

		top = new TimelineObsPanel(controler);

		this.add(authorBox);
		this.add(top);
	}
}
