package Graphic;

import java.awt.BorderLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import App.Controler;
import Data.Data;
import Data.Tweet;

public class TweetObsPanel extends JPanel implements Observer {
	private JTextArea textArea;

	public TweetObsPanel(Controler controler) {
		super(new BorderLayout());

		controler.getData().addObserver(this);

		JTextArea ta = new JTextArea();
		this.textArea = ta;
		textArea.setFocusable(false);
		JScrollPane jp = new JScrollPane(textArea);
		jp.setBorder(BorderFactory.createTitledBorder("Tweets en direct:"));
		this.add(jp);
	}

	public void write(Tweet tweet) {

		textArea.append(tweet.toString());
	}

	public void update(Observable obs, Object obj) {
		this.write(Data.getInstance().getLastTweet());
	}
}
