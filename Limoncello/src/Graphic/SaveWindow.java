package Graphic;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import App.Controler;

public class SaveWindow extends JFrame {

	public void kill() {
		this.dispose();
	}

	private JTextField textField;
	private boolean bd;
	private Controler controler;

	public SaveWindow(final Controler controler, boolean db) {
		this.controler = controler;
		this.setSize(280, 280);
		this.setVisible(true);
		this.bd = bd;

		JPanel UpPanel = new JPanel();
		JLabel instruction = new JLabel("Entrez un nom (sans extension)");
		UpPanel.add(instruction, BorderLayout.NORTH);

		JPanel BottomPanel = new JPanel();
		JButton Ok = new JButton("Save");
		JButton Cancel = new JButton("Cancel");
		BottomPanel.add(Ok, BorderLayout.SOUTH);
		BottomPanel.add(Cancel, BorderLayout.SOUTH);

		JTextField tf = new JTextField(10);
		this.textField = tf;
		this.add(textField, BorderLayout.NORTH);
		this.add(UpPanel, BorderLayout.CENTER);
		this.add(BottomPanel, BorderLayout.SOUTH);

		textField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controler.Save(textField.getText(), bd);

				kill();
			}
		});

		Ok.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controler.Save(textField.getText(), bd);
				kill();
			}
		});

		Cancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				kill();
			}
		});
	}
}
