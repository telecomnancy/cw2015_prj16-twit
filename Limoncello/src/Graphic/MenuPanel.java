package Graphic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import App.Controler;

public class MenuPanel extends JPanel {
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem loadM;
	private JMenuItem saveDBM;
	private JMenuItem saveCSVM;
	private Controler controler;

	public MenuPanel(final Controler controler) {
		this.controler = controler;
		JMenuBar MenuBar = new JMenuBar();
		JMenu Menu = new JMenu("Menu");
		JMenuItem Load = new JMenuItem("Load");
		JMenuItem SaveCSV = new JMenuItem("Save (CSV)");
		JMenuItem SaveDB = new JMenuItem("Save (.db)");
		this.saveCSVM = SaveCSV;
		this.saveDBM = SaveDB;
		this.loadM = Load;
		this.menu = Menu;
		this.menuBar = MenuBar;

		// saveDBM.setEnabled(false);
		// saveCSVM.setEnabled(false);

		saveCSVM.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new SaveWindow(controler, false);
			}
		});

		saveDBM.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new SaveWindow(controler, true);
			}
		});

		loadM.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new LoadWindow();
			}
		});

		this.menu.add(loadM);
		this.menu.add(saveDBM);
		this.menu.add(saveCSVM);
		this.menuBar.add(menu);
		this.add(menuBar);
	}
}
