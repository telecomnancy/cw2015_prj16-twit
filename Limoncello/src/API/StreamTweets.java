package API;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import App.Main;
import Data.Data;
import Data.ModelParameter;
import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;

public class StreamTweets extends Collect {

	private List<Status> tweets = new ArrayList<Status>();

	public StreamTweets(final TwitterStream twitterStream, Data data) {

		long startTime = System.currentTimeMillis();
		long elapsedTime = 0;
		long timer = ModelParameter.getInstance().getTimer();

		StatusListener listener = new StatusListener() {

			@Override
			public void onException(Exception e) {
				System.out.println("Exception occured:" + e.getMessage());

				e.printStackTrace();
			}

			@Override
			public void onTrackLimitationNotice(int n) {
				System.out.println("Track limitation notice for " + n);
			}

			@Override
			public void onStatus(Status status) {
				tweets.add(status);
				if (ModelParameter.getInstance().getVisibleConsole()) {
					System.out.println(status.getUser().getScreenName() + " a tweeté " + status.getText());
				}
			}

			@Override
			public void onStallWarning(StallWarning arg0) {
				System.out.println("Stall warning");
			}

			@Override
			public void onScrubGeo(long arg0, long arg1) {
				System.out.println("Scrub geo with:" + arg0 + ":" + arg1);
			}

			@Override
			public void onDeletionNotice(StatusDeletionNotice arg0) {
				System.out.println("Status deletion notice");
			}
		};

		FilterQuery qry = new FilterQuery();
		String text = ModelParameter.getInstance().getFilterField(true, Main.TEXT);
		String[] texts = text.split(",");

		String[] hash = ModelParameter.getInstance().getFilterField(true, Main.HASH).split(",");
		String[] keywords = new String[texts.length + hash.length];
		if (!hash[0].equals("")) {
			for (int i = 0; i < hash.length; i++) {
				if (!texts[0].equals("")) {
					keywords[i] = texts[i];
					keywords[texts.length + i] = "#" + hash[i];
				} else
					keywords[i] = "#" + hash[i];
			}
		} else if (!texts[0].equals("")) {
			keywords = texts;
		} else {
			keywords[0] = "codingweek";
			keywords[1] = "";
		}
		String[] language = ModelParameter.getInstance().getFilterField(true, Main.LANG).split(",");
		qry.track(keywords);
		qry.language(language);

		twitterStream.addListener(listener);
		twitterStream.filter(qry);
		while (elapsedTime < timer * 60 * 1000) {
			elapsedTime = (new Date()).getTime() - startTime;
		}
		twitterStream.shutdown();

	}

	public List<Status> getTweets() {
		return tweets;
	}
}
