package API;

import java.io.IOException;
import java.util.Map;

import Data.Data;
import twitter4j.RateLimitStatus;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;

public class MainAPI {
	private Collect collection;

	public MainAPI(Boolean isStream) {
		if (!isStream) {
			Twitter twitter = null;
			try {
				twitter = Authentification.authentification();
			} catch (TwitterException e) {
				e.printStackTrace();
			}

			Map<String, RateLimitStatus> rateLimitStatus = null;
			try {
				rateLimitStatus = twitter.getRateLimitStatus("search");
			} catch (TwitterException e) {
				e.printStackTrace();
			}
			RateLimitStatus searchTweetsRateLimit = rateLimitStatus.get("/search/tweets");

			System.out.printf("You have %d calls remaining out of %d, Limit resets in %d seconds\n",
					searchTweetsRateLimit.getRemaining(), searchTweetsRateLimit.getLimit(),
					searchTweetsRateLimit.getSecondsUntilReset());
			try {
				collection = new Collect(twitter);
			} catch (TwitterException | IOException e) {
				e.printStackTrace();
			}
		} else {
			TwitterStream twitterStream = null;
			try {
				twitterStream = Authentification.authentificationStream();
			} catch (TwitterException e) {
				e.printStackTrace();
			}
			try {
				collection = new Collect(twitterStream);
			} catch (TwitterException | IOException e) {
				e.printStackTrace();
			}
		}
	}

	public Data extractData() {
		return collection.getData();
	}

}
