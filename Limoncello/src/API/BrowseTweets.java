package API;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import App.Main;
import Data.Data;
import Data.ModelParameter;

public class BrowseTweets extends Collect {

	private List<Status> tweets = new ArrayList<Status>();

	public List<Status> getTweets() {
		return tweets;
	}

	public BrowseTweets(Twitter twitter, Data data) throws IOException {
		try {
			String text = "";
			String lang = "";

			Query query = new Query("codingweek");
			query.setCount(100);

			if (!ModelParameter.getInstance().getFilterField(true, Main.TEXT)
					.isEmpty()) {
				text = ModelParameter.getInstance().getFilterField(true,
						Main.TEXT);
				query.setQuery(text);
			}

			if (!ModelParameter.getInstance().getFilterField(true, Main.LANG)
					.isEmpty()) {
				lang = ModelParameter.getInstance().getFilterField(true,
						Main.LANG);
				query.lang(lang);
			}

			if (!ModelParameter.getInstance().getFilterField(true, Main.HASH)
					.isEmpty()) {
				text = ModelParameter.getInstance().getFilterField(true,
						Main.HASH);
				query.setQuery("#" + text);
			}

			QueryResult result;
			int compteur = 1;
			do {
				result = twitter.search(query);
				for (Status status : result.getTweets()) {
					tweets.add(status);
				}
				compteur += 100;

			} while ((query = result.nextQuery()) != null
					&& compteur < ModelParameter.getInstance().getNB_TWEETS());
		} catch (TwitterException te) {
			te.printStackTrace();
			System.out.println("Failed to search tweets: " + te.getMessage());
			System.exit(-1);
		}

	}
}
