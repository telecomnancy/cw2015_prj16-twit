package API;

import App.Controler;
import java.io.IOException;

import twitter4j.TwitterException;
import App.Main;
import Data.Author;
import Data.Data;
import Data.ModelParameter;
import Data.Tweet;
import JDBC.SQLiteJDBC;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import twitter4j.Status;
import twitter4j.Twitter;

public class TestAPI {

	private static boolean isStream = false;

	public static void main(String[] argc) throws TwitterException, IOException {
		ModelParameter parameter = ModelParameter.getInstance();
                
                Data modele = Data.getInstance();
                Controler controler = new Controler();
                
		for (int i = 0; i < 12; i++) {
			ModelParameter.getInstance().setFieldKept(true, i, true);
		}
		for (int i = 0; i < 5; i++) {
			ModelParameter.getInstance().setFieldKept(false, i, true);
		}
		ModelParameter.getInstance().setFilterField(true, Main.LANG, "fr, en");
                ModelParameter.getInstance().setVisibleConsole(false);
                ModelParameter.getInstance().setTimer(1);
                ModelParameter.getInstance().setNB_TWEETS(100);
               List<Tweet> tweets = new ArrayList<Tweet>();//modele.getTweets();
                
                //test Stream
                
                isStream = true;
                System.out.println("On récupère maintenant un stream avec comme filtres les mots Obama, Trump et ISIS et le hashtag #COP21 sur des tweets en anglais.\n Le stream dure une minute.");
                ModelParameter.getInstance().setFilterField(true, Main.TEXT,
				"Obama, Trump");
		ModelParameter.getInstance().setFilterField(true, Main.HASH,
				"COP21");
		ModelParameter.getInstance().setFilterField(true, Main.LANG, "en");

                new SQLiteJDBC().store(new MainAPI(isStream).extractData());
                System.out.println("On vérifie maintenant que les tweets récupérés contiennent l'un des mots ou le hashtag choisi.");
                tweets = modele.getTweets();
                String trump = "Trump";
                String cop21 = "#COP21";
                String obama = "Obama";
                Pattern p1 = Pattern.compile(obama);
                Pattern p2 = Pattern.compile(trump);
                Pattern p4 = Pattern.compile(cop21);
                Boolean ok = true;
                String text = "";
                String buffer = "";
                for(Tweet tweet : tweets) {
                    buffer = tweet.getBody();
                    int longueur = buffer.length();
                    for (int i = 0; i < longueur; i++)
                    {
                        
			if (buffer.charAt(i) == '*') {
                            text = text + "";
                        }else text = text + buffer.charAt(i);
                    }
                    Matcher m1 = p1.matcher(text);
                    Matcher m2 = p2.matcher(text);
                    Matcher m4 = p4.matcher(text);
                    
                    if(m1.find() || m2.find() || m4.find())
                    {
                        //System.out.println(text);
                    }else ok = false;
                }
                if(ok)
                {
                    System.out.println("Success!");
                }else System.out.println("Nul.");
                
                /*List<Author> authors = data.getAuthors();
                Author author = authors.get(3);
                author.setTimeline();
                List<Tweet> tweets = author.getTimeline();
                */
	}

}
