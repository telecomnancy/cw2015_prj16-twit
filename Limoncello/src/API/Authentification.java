package API;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;

public class Authentification {

	private static String consumerKey = "";

	private static String consumerSecret = "";

	private static String accessToken = "";

	private static String accessTokenSecret = "";

	public static Twitter authentification() throws TwitterException {

		Properties prop = new Properties();
		InputStream input = null;

		try {
			input = new FileInputStream("config.properties");
		} catch (FileNotFoundException ex) {
			Logger.getLogger(Authentification.class.getName()).log(
					Level.SEVERE, null, ex);
		}
		try {
			prop.load(input);
		} catch (IOException ex) {
			Logger.getLogger(Authentification.class.getName()).log(
					Level.SEVERE, null, ex);
		}
		if (input != null) {
			try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		consumerKey = prop.getProperty("consumerKey");
		consumerSecret = prop.getProperty("consumerSecret");
		accessToken = prop.getProperty("accessToken");
		accessTokenSecret = prop.getProperty("accessTokenSecret");

		ConfigurationBuilder cb = new ConfigurationBuilder();

		cb.setOAuthConsumerKey(consumerKey);
		cb.setOAuthConsumerSecret(consumerSecret);

		Twitter twitter = new TwitterFactory(cb.build()).getInstance();

		twitter.setOAuthAccessToken(new AccessToken(accessToken,
				accessTokenSecret));

		return twitter;
	}

	public static TwitterStream authentificationStream()
			throws TwitterException {

		Properties prop = new Properties();
		InputStream input = null;

		try {
			input = new FileInputStream("config.properties");
		} catch (FileNotFoundException ex) {
			Logger.getLogger(Authentification.class.getName()).log(
					Level.SEVERE, null, ex);
		}
		try {
			prop.load(input);
		} catch (IOException ex) {
			Logger.getLogger(Authentification.class.getName()).log(
					Level.SEVERE, null, ex);
		}
		if (input != null) {
			try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		consumerKey = prop.getProperty("consumerKey");
		consumerSecret = prop.getProperty("consumerSecret");
		accessToken = prop.getProperty("accessToken");
		accessTokenSecret = prop.getProperty("accessTokenSecret");

		ConfigurationBuilder cb = new ConfigurationBuilder();

		cb.setOAuthConsumerKey(consumerKey);
		cb.setOAuthConsumerSecret(consumerSecret);

		TwitterStream twitterStream = new TwitterStreamFactory(cb.build())
				.getInstance();

		twitterStream.setOAuthAccessToken(new AccessToken(accessToken,
				accessTokenSecret));

		return twitterStream;
	}
}