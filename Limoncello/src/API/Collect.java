package API;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import App.Main;
import Data.Author;
import Data.Data;
import Data.ModelParameter;
import Data.Tweet;
import twitter4j.GeoLocation;
import twitter4j.MediaEntity;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;

public class Collect {

	Data data;

	public Collect() {
		data = Data.getInstance();
	}

	public Collect(Twitter twitter) throws TwitterException, IOException {

		List<Status> tweets = new ArrayList<Status>();
		data = Data.getInstance();

		tweets = new BrowseTweets(twitter, data).getTweets();
		extractData(tweets);

	}

	public Collect(TwitterStream twitterStream) throws TwitterException, IOException {

		List<Status> tweets = new ArrayList<Status>();
		data = Data.getInstance();
		tweets = new StreamTweets(twitterStream, data).getTweets();
		extractData(tweets);

	}

	public List<Tweet> getTimeline(long id) {
		List<Tweet> tweets = new ArrayList<Tweet>();
		List<Status> twits = null;
		Twitter twitter = null;
		try {
			twitter = Authentification.authentification();
		} catch (TwitterException ex) {
			Logger.getLogger(Collect.class.getName()).log(Level.SEVERE, null, ex);

		}
		try {
			twits = twitter.getUserTimeline(id);
		} catch (TwitterException ex) {

			Logger.getLogger(Collect.class.getName()).log(Level.SEVERE, null, ex);

		}
		for (Status twit : twits) {
			Tweet tweet = new Tweet(twit.getId());
			tweet.setBody(twit.getText());
			java.util.Date date1 = twit.getCreatedAt();
			java.sql.Date date = new java.sql.Date(date1.getTime());
			tweet.setDate(date);
			tweet.setFavoriteCount(twit.getFavoriteCount());
			tweet.setLang(twit.getLang());

			String[] resHash = new String[twit.getHashtagEntities().length];
			for (int x = 0; x < twit.getHashtagEntities().length; x++) {
				resHash[x] = twit.getHashtagEntities()[x].getText();
			}
			tweet.setHashtag(resHash);

			tweet.setGeo(twit.getGeoLocation());
			tweet.setInReplyTo(twit.getInReplyToStatusId());
			tweet.setRetweetCount(twit.getRetweetCount());

			Author author = new Author(twit.getUser().getId());
			Author RTauthor;

			author.setName(twit.getUser().getScreenName());
			author.setLanguage(twit.getUser().getLang());
			author.setNbFollowed(twit.getUser().getFriendsCount());
			author.setNbFollowers(twit.getUser().getFollowersCount());

			if (twit.getRetweetedStatus() != null) {
				RTauthor = new Author(twit.getRetweetedStatus().getUser().getId());
				RTauthor.setName(twit.getRetweetedStatus().getUser().getScreenName());
				RTauthor.setLanguage(twit.getRetweetedStatus().getUser().getLang());
				RTauthor.setNbFollowed(twit.getRetweetedStatus().getUser().getFriendsCount());
				RTauthor.setNbFollowers(twit.getRetweetedStatus().getUser().getFollowersCount());
				tweet.setRTauthor(RTauthor);
			}

			tweet.setAuthor(author);

			tweets.add(tweet);
		}
		return tweets;
	}

	public void extractData(List<Status> tweets) {
		String text = "";
		String lang = "";
		int compteur = 1;
		for (Status tweet : tweets) {
			Author author = new Author(tweet.getUser().getId());
			Author RTauthor = new Author(-1);
			if (tweet.getRetweetedStatus() != null)
				RTauthor.setId(tweet.getRetweetedStatus().getUser().getId());
			Tweet tweet2 = new Tweet(tweet.getId());

			if (ModelParameter.getInstance().getFieldKept(false, Main.NAME)) {
				author.setName(tweet.getUser().getScreenName());
				if (tweet.getRetweetedStatus() != null)
					RTauthor.setName(tweet.getRetweetedStatus().getUser().getScreenName());
			}

			if (ModelParameter.getInstance().getFieldKept(false, Main.LANG)) {
				author.setLanguage(tweet.getUser().getLang());
				if (tweet.getRetweetedStatus() != null)
					RTauthor.setLanguage(tweet.getRetweetedStatus().getUser().getLang());
			}

			if (ModelParameter.getInstance().getFieldKept(false, Main.FOLLOWERS_CNT)) {
				// get number of followers
				author.setNbFollowers(tweet.getUser().getFollowersCount());
				if (tweet.getRetweetedStatus() != null)
					RTauthor.setNbFollowers(tweet.getRetweetedStatus().getUser().getFollowersCount());
				/*
				 * try {
				 * 
				 * // get list of followers List<Author> followers = new
				 * ArrayList<Author>();
				 * 
				 * long cursor = -1; PagableResponseList<User> pagableFollowers;
				 * do { pagableFollowers = twitter.getFollowersList(
				 * author.getId(), cursor); for (User user : pagableFollowers) {
				 * followers.add(new Author(user.getId())); } } while ((cursor =
				 * pagableFollowers.getNextCursor()) != 0);
				 * 
				 * author.setFollowers(followers); } catch (TwitterException e)
				 * { // System.out.println("rip rate limit (follower)"); }
				 */

			}

			if (ModelParameter.getInstance().getFieldKept(false, Main.FOLLOW_CNT)) {
				author.setNbFollowed(tweet.getUser().getFriendsCount());
				if (tweet.getRetweetedStatus() != null)
					RTauthor.setNbFollowed(tweet.getRetweetedStatus().getUser().getFriendsCount());

				/*
				 * try { // get number of follow author.setNbFollowed(tweet
				 * .getUser().getFriendsCount()); // get list of follow
				 * List<Author> follows = new ArrayList<Author>();
				 * 
				 * long cursor = -1; PagableResponseList<User> pagableFollowed;
				 * do { pagableFollowed = twitter.getFriendsList(
				 * author.getId(), cursor); for (User user : pagableFollowed) {
				 * follows.add(new Author(user.getId())); } } while ((cursor =
				 * pagableFollowed.getNextCursor()) != 0);
				 * 
				 * author.setFollowed(follows); } catch (TwitterException e) {
				 * // System.out.println("rip rate limit (follow)"); }
				 */
			}

			if (ModelParameter.getInstance().getFieldKept(true, Main.DATE)) {
				java.util.Date date1 = tweet.getCreatedAt();
				java.sql.Date date = new java.sql.Date(date1.getTime());
				tweet2.setDate(date);
			}

			if (ModelParameter.getInstance().getFieldKept(true, Main.TEXT)) {
				String buffer = tweet.getText();
				if (tweet.isRetweet()) {
					String[] buffer2 = buffer.split("RT @\\p{Alnum}*\\p{Punct}?\\p{Alnum}*:?");
					buffer = buffer2[1];
				}
				text = "";
				int longueur = buffer.length();

				for (int i = 0; i < longueur; i++) // faut doubler les
													// apostrophes pour
													// que sql soit
													// content
				{
					text = text + buffer.charAt(i);
					if (buffer.charAt(i) == '\'')
						text = text + '\'';
				}
				tweet2.setBody(text);
			}

			if (ModelParameter.getInstance().getFieldKept(true, Main.LANG)) {
				tweet2.setLang(tweet.getLang());
			}

			if (ModelParameter.getInstance().getFieldKept(true, Main.RT_ID) && tweet.isRetweet()) {
				tweet2.setRTauthor(RTauthor);
			}

			if (ModelParameter.getInstance().getFieldKept(true, Main.GEOLOC)) {
				GeoLocation geoloc = tweet.getGeoLocation();
				tweet2.setGeo(geoloc);
			}

			if (ModelParameter.getInstance().getFieldKept(true, Main.AUTHOR)) {
				tweet2.setAuthor(author);
			}

			if (ModelParameter.getInstance().getFieldKept(true, Main.FAV_COUNT)) {
				tweet2.setFavoriteCount(tweet.getFavoriteCount());
			}
			if (ModelParameter.getInstance().getFieldKept(true, Main.RT_COUNT)) {
				tweet2.setRetweetCount(tweet.getRetweetCount());
			}

			if (ModelParameter.getInstance().getFieldKept(true, Main.REPLY_ID)) {
				tweet2.setInReplyTo(tweet.getInReplyToStatusId());
			}

			if (ModelParameter.getInstance().getFieldKept(true, Main.HASH)) {
				String[] resHash = new String[tweet.getHashtagEntities().length];
				for (int x = 0; x < tweet.getHashtagEntities().length; x++) {
					resHash[x] = tweet.getHashtagEntities()[x].getText();
				}
				tweet2.setHashtag(resHash);
			}

			if (ModelParameter.getInstance().getFieldKept(true, Main.IMG)) {
				MediaEntity[] medias = tweet.getMediaEntities();
				String mediaUrl;
				String mediasPath[] = new String[medias.length];
				if (medias.length != 0) {
					for (int i = 0; i < medias.length; i++) {
						mediaUrl = medias[i].getMediaURL();
						String destinationFile = tweet.getId() + "[" + i + "]" + ".jpg";
						mediasPath[i] = "Medias/" + destinationFile;
						try {
							saveImage(mediaUrl, destinationFile);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					tweet2.setMediasPath(mediasPath);
				}
			}
			data.addTweet(tweet2);
			data.addAuthor(author);
			compteur++;
		}
		System.out.println(compteur - 1 + " tweets collectés.");
	}

	public Data getData() {
		return data;
	}

	public void saveImage(String imageUrl, String destinationFile) throws IOException {
		URL url = new URL(imageUrl);
		InputStream is = url.openStream();
		new File("Medias").mkdir();
		OutputStream os = new FileOutputStream("Medias/" + destinationFile);

		byte[] b = new byte[2048];
		int length;

		while ((length = is.read(b)) != -1) {
			os.write(b, 0, length);
		}

		is.close();
		os.close();
	}

}