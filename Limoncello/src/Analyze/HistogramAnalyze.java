package Analyze;

import java.util.ArrayList;

import Data.Data;
import Data.Tweet;

public class HistogramAnalyze extends Analyze {
	private ArrayList<FieldParameter> param;
	private double coef;

	public HistogramAnalyze() {
		coef = 1;
		param = new ArrayList<FieldParameter>();
	}

	@Override
	public Results execute() {
		HistogramResults histoResults = new HistogramResults();
		Object object;
		Object[] objects;
		if (testParam()) {
			for (Tweet t : Data.getInstance().getTweets()) {
				if (param.size() > 1)
					try {
						if (param.get(1).isTweetParameter())
							coef = Double.parseDouble("" + t.getInt(param.get(1).getField()));
						else
							coef = Double.parseDouble("" + t.getAuthor().getInt(param.get(1).getField()));
					} catch (Exception e) {
						System.err
								.println("" + t.getInt(param.get(1).getField()).getClass() + "can't be cast in double");
					}

				object = t.getInt(param.get(0).getField());
				if (object != null) {
					if (object instanceof Object[]) {
						objects = (Object[]) object;
						for (Object o : objects) {
							histoResults.addOccurence(o.toString(), coef);
						}
					} else
						histoResults.addOccurence(object.toString(), coef);
				}
			}
		}
		return histoResults;
	}

	public boolean testParam() {
		switch (param.size()) {
		case (0):
			System.out.println("not enough parameters");
			return false;
		case (1):
			return param.get(0).isValid();
		default:
			if (param.get(1).isValid() && param.get(1).isNumeric())
				return true;
			return false;
		}

	}

	@Override
	public void setParameter(int index, AnalyzeParameter p) {
		try {
			FieldParameter f = (FieldParameter) p;
			switch (param.size()) {
			case 0:
				if (index == 0)
					param.add(f);
				if (index == 1)
					System.out.println("first choose a first parameter");
				break;
			case 1:
				if (index == 0)
					param.set(0, f);
				if (index == 1)
					param.add(f);
				break;
			default:
				if (index == 0)
					param.set(0, f);
				if (index == 1)
					param.set(1, f);
				break;

			}
		} catch (Exception e) {
			System.out.println("analyzeParameter can't be considerd as FieldParameter");
		}

	}

	@Override
	public void removeParameters() {
		param = new ArrayList<FieldParameter>();
	}

	@Override
	public int getNbOfParameters() {
		return param.size();
	}

}
