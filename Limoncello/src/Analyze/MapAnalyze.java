package Analyze;

import Data.Data;
import Data.Tweet;

public class MapAnalyze extends Analyze {
	private MapResult mapresult;

	@Override
	public Results execute() {
		mapresult = new MapResult();
		for (Tweet t : Data.getInstance().getTweets()) {
			if (t.getGeo() != null) {
				mapresult.addPlaces(t.getGeo().getLatitude(), t.getGeo().getLongitude());
			}
		}
		return mapresult;
	}

}
