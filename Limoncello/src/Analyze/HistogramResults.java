package Analyze;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class HistogramResults implements Results {
	private List<Double> results;
	private List<String> columns;

	public HistogramResults() {
		results = new ArrayList<Double>();
		columns = new ArrayList<String>();
	}

	public void addOccurence(String occurence, Double coef) {
		int i = columns.indexOf(occurence);
		if (i == -1) {
			columns.add(occurence);
			results.add(coef);
		} else {
			results.set(i, results.get(i) + coef);
		}
	}

	public Component display() {
		DefaultCategoryDataset datatest = new DefaultCategoryDataset();

		for (int i = 0; i < results.size(); i++) {
			datatest.addValue(results.get(i), "", columns.get(i));
		}

		String plotTitle = "";
		String xAxis = "";
		String yAxis = "";
		PlotOrientation orientation = PlotOrientation.VERTICAL;

		boolean show = false;
		boolean toolTips = false;
		boolean urls = false;
		JFreeChart chart = ChartFactory.createBarChart(plotTitle, xAxis, yAxis, datatest, PlotOrientation.HORIZONTAL,
				show, toolTips, urls);
		return new ChartPanel(chart);
	}
}
