package Analyze;

public abstract class Analyze {

	public Results execute() {
		return null;
	} // must be override

	public void setParameter(int index, AnalyzeParameter f) {
	} // must be override

	public void removeParameters() {
	} // must be override

	public int getNbOfParameters() {
		return 0;
	} // must be override
}
