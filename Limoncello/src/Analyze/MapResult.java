package Analyze;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;

import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;

public class MapResult implements Results {
	List<Double> latitudes;
	List<Double> longitudes;

	public MapResult() {
		latitudes = new ArrayList<Double>();
		longitudes = new ArrayList<Double>();
	}

	public Component display() {
		int i = 0;
		if (latitudes != null) {
			JMapViewer map = new org.openstreetmap.gui.jmapviewer.JMapViewer();
			for (i = 0; i < latitudes.size(); i++) {
				map.addMapMarker(new MapMarkerDot(latitudes.get(i), longitudes.get(i)));
			}
			return map;
		} else {
			return new JLabel("Trop peu de tweet renseignant leur géolocalisation");
		}
	}

	public void addPlaces(double lati, double longi) {
		latitudes.add(lati);
		longitudes.add(longi);
	}

}
