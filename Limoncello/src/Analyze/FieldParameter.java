package Analyze;

import Data.ModelParameter;

public class FieldParameter extends AnalyzeParameter {
	private int field;
	private boolean isTweetParameter;

	public boolean isTweetParameter() {
		return isTweetParameter;
	}

	public void setTweetParameter(boolean isTweetParameter) {
		this.isTweetParameter = isTweetParameter;
	}

	public FieldParameter(String label) {
		super(label);
	}

	public int getField() {
		return field;
	}

	public void setField(int field) {
		this.field = field;
	}

	public boolean isValid() {
		return ModelParameter.getInstance().getFieldKept(isTweetParameter, field);
	}

	public boolean isNumeric() {
		if (isTweetParameter) {
			switch (field) {
			case 10:
				return true;
			case 11:
				return true;
			default:
				return false;
			}
		} else {
			switch (field) {
			case 3:
				return true;
			case 4:
				return true;
			default:
				return false;
			}
		}
	}

}
