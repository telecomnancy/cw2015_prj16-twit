package GraphicAnalyze;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

import App.Controler;

public class AbstractAnalyzePanel extends JPanel implements Observer {

	protected JPanel resultView;
	protected JPanel parametersSelection;
	protected JButton launch;
	protected Controler controler;

	public AbstractAnalyzePanel(Controler controler) {
		super(new BorderLayout());

		this.controler = controler;
		controler.getCAL().addObserver(this);

		createParametersSelection();
		parametersSelection.setPreferredSize(new Dimension(900, 100));
		this.add(parametersSelection, BorderLayout.NORTH);

		launch = new JButton("Launch");
		launch.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				controler.lauchnAnalyze();
			}
		});
		launch.setPreferredSize(new Dimension(900, 50));
		this.add(launch, BorderLayout.SOUTH);

		resultView = new JPanel();
		resultView.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 2, true));
		add(resultView, BorderLayout.CENTER);

	}

	public void createParametersSelection() {
		parametersSelection = new JPanel();
		add(parametersSelection);
	} // must be overrode

	public void update(Observable obs, Object obj) {
		Component component = controler.getCAL().getResultDisplay();
		component.setPreferredSize(new Dimension(900, 700));
		this.remove(resultView);
		resultView = new JPanel();
		resultView.add(component);
		resultView.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 2, true));
		this.add(resultView, BorderLayout.CENTER);
	}

}