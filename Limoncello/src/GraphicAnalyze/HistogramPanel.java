package GraphicAnalyze;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Analyze.FieldParameter;
import App.Controler;
import App.Main;

public class HistogramPanel extends AbstractAnalyzePanel {
	JPanel firstParam;
	JPanel secondParam;

	public HistogramPanel(Controler controler) {
		super(controler);
	}

	@Override
	public void createParametersSelection() {
		parametersSelection = new JPanel();

		firstParam = new JPanel(new GridLayout(2, 1));

		firstParam.add(new JLabel("Separateur de classes"));

		String[] firstFieldLabel = { " ", "ID", "Language", "Date", "Hashtag", "ReTweet Author", "In ReplyTo",
				"Author ID" };
		JComboBox firstFieldCombo = new JComboBox(firstFieldLabel);
		firstFieldCombo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FieldParameter field = new FieldParameter((String) firstFieldCombo.getSelectedItem());
				field.setTweetParameter(true);
				switch (firstFieldCombo.getSelectedIndex()) {
				case 1:
					field.setField(Main.ID);
					controler.setAnalyzeParameter(0, field);
					break;
				case 2:
					field.setField(Main.LANG);
					controler.setAnalyzeParameter(0, field);
					break;
				case 3:
					field.setField(Main.DATE);
					controler.setAnalyzeParameter(0, field);
					break;
				case 4:
					field.setField(Main.HASH);
					controler.setAnalyzeParameter(0, field);
					break;
				case 5:
					field.setField(Main.RT_ID);
					controler.setAnalyzeParameter(0, field);
					break;
				case 6:
					field.setField(Main.REPLY_ID);
					controler.setAnalyzeParameter(0, field);
					break;
				case 7:
					field.setTweetParameter(false);
					field.setField(Main.ID);
					controler.setAnalyzeParameter(0, field);
					break;
				default:
					break;
				}

			}
		});

		firstParam.add(firstFieldCombo);

		secondParam = new JPanel(new GridLayout(2, 1));

		secondParam.add(new JLabel("Nombre de ...                              par ..."));

		String[] secondFieldLabel = { "occurence", "Retweet count", "Favorite count (très souvent null)",
				"author followers", "author friends" };
		JComboBox secondFieldCombo = new JComboBox(secondFieldLabel);
		secondFieldCombo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FieldParameter field = new FieldParameter((String) secondFieldCombo.getSelectedItem());
				field.setTweetParameter(true);
				switch (secondFieldCombo.getSelectedIndex()) {
				case 1:
					field.setField(Main.RT_COUNT);
					controler.setAnalyzeParameter(1, field);
					break;
				case 2:
					field.setField(Main.FAV_COUNT);
					controler.setAnalyzeParameter(1, field);
					break;
				case 3:
					field.setTweetParameter(false);
					field.setField(Main.FOLLOWERS_CNT);
					controler.setAnalyzeParameter(1, field);
					break;
				case 4:
					field.setTweetParameter(false);
					field.setField(Main.FOLLOW_CNT);
					controler.setAnalyzeParameter(1, field);
					break;
				default:
					break;
				}

			}
		});

		secondParam.add(secondFieldCombo);

		parametersSelection.add(secondParam);
		parametersSelection.add(firstParam);

	}

}
