package App;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import loadAndSave.Load;
import loadAndSave.SaveDB;
import Data.ModelParameter;

public class MainConsole {
	public static void lancerConsole() {
		Controler controler = new Controler();
		Boolean end = false;
		while (!end) {
			System.out.println("Load, Browse or Stream ?");
			String entree = lireString();
			if (entree.equalsIgnoreCase("Load")) {
				Load();
				end = true;
			}
			if (entree.equalsIgnoreCase("Browse")) {
				Browse(controler);
				end = true;
			}
			if (entree.equalsIgnoreCase("Stream")) {
				Stream(controler);
				end = true;
			}
		}
	}

	public static void Load() {
		choixSave();
	}

	public static void Stream(Controler controler) {
		choixDonnees();
		System.out
				.println("Voulez-vous trier les tweets en fonction d'un mot-clé ? (y/n)");
		String entree = lireString();
		entree = boucleVerif(entree);
		if (entree.equalsIgnoreCase("y")) {
			System.out
					.println("Entrez la chaîne de caractères sur laquelle vous souhaitez effectuer votre recherche :");
			entree = lireString();
			ModelParameter.getInstance()
					.setFilterField(true, Main.TEXT, entree);
		}
		System.out
				.println("Voulez-vous trier les tweets en fonction de leurs hashtags ? (y/n)");
		entree = lireString();
		entree = boucleVerif(entree);
		if (entree.equalsIgnoreCase("y")) {
			System.out
					.println("Entrez le hashtag sur laquelle vous souhaitez effectuer votre recherche :");
			entree = lireString();
			ModelParameter.getInstance()
					.setFilterField(true, Main.HASH, entree);
		}
		choixFiltres();
		System.out.println("Durée de la collecte en minutes :");
		Boolean ok = false;
		while (!ok) {
			entree = lireString();
			try {
				long test = Long.parseLong(entree);
				ModelParameter.getInstance().setFilterField(true,
						ModelParameter.getInstance().getNB_TWEETS(), entree);
				ok = true;
			} catch (NumberFormatException nfe) {
				System.out.println("Ce n'est pas un entier espèce de vilain");
			}
		}
		System.out
				.println("Voulez-vous visualiser les tweets en direct sur la console ? (y/n)");
		entree = lireString();
		entree = boucleVerif(entree);
		if (entree.equalsIgnoreCase("y")) {
			ModelParameter.getInstance().setVisibleConsole(true);
		}
		controler.setBackToTheFuture(true);
		controler.extractAndStore();
		System.out
				.println("Maintenant tu peux aller regarder la DB comme un grand parce que la vie");
		System.out.println("Voulez-vous sauvegarder cette collecte ? (y/n)");
		entree = lireString();
		entree = boucleVerif(entree);
		if (entree.equalsIgnoreCase("y")) {
			System.out.println("Nom de la sauvegarde ?");
			new SaveDB(lireString());
		}

	}

	public static void Browse(Controler controler) {
		choixDonnees();
		System.out
				.println("Entrez le hashtag ou la chaîne de caractères sur laquelle vous souhaitez effectuer votre recherche :");
		String entree = lireString();
		ModelParameter.getInstance().setFilterField(true, Main.TEXT, entree);
		choixFiltres();
		controler.setBackToTheFuture(false);
		controler.extractAndStore();
		System.out
				.println("Maintenant tu peux aller regarder la DB comme un grand parce que la vie");
		System.out.println("Voulez-vous sauvegarder cette collecte ? (y/n)");
		entree = lireString();
		entree = boucleVerif(entree);
		if (entree.equalsIgnoreCase("y")) {
			System.out.println("Nom de la sauvegarde ?");
			entree = lireString();
			new SaveDB(entree);
		}
	}

	public static void choixFiltres() {
		System.out
				.println("Voulez-vous trier les tweets en fonction de leurs langues ? (y/n)");
		String entree = lireString();
		entree = boucleVerif(entree);
		if (entree.equalsIgnoreCase("y")) {
			System.out
					.println("Entrez la langue (ex : fr, en, de...) sur laquelle vous souhaitez effectuer votre recherche :");
			entree = lireString();
			ModelParameter.getInstance()
					.setFilterField(true, Main.LANG, entree);
		}
	}

	public static void choixSave() {

		System.out.println("Entrez le nom du fichier");
		String entree = lireString();
		new Load(entree);

	}

	public static void choixDonnees() {
		System.out
				.println("Choisissez les données que vous souhaitez conserver : (y/n)");
		System.out.println("Langue");
		String entree = lireString();
		entree = boucleVerif(entree);
		if (entree.equalsIgnoreCase("y")) {
			ModelParameter.getInstance().setFieldKept(true, Main.LANG, true);
		} else
			ModelParameter.getInstance().setFieldKept(true, Main.LANG, false);
		System.out.println("Texte");
		entree = lireString();
		entree = boucleVerif(entree);
		if (entree.equalsIgnoreCase("y")) {
			ModelParameter.getInstance().setFieldKept(true, Main.TEXT, true);
		} else
			ModelParameter.getInstance().setFieldKept(true, Main.TEXT, false);
		System.out.println("Nombre de favoris");
		entree = lireString();
		entree = boucleVerif(entree);
		if (entree.equalsIgnoreCase("y")) {
			ModelParameter.getInstance().setFieldKept(true, Main.FAV_COUNT,
					true);
		} else
			ModelParameter.getInstance().setFieldKept(true, Main.FAV_COUNT,
					false);
		System.out.println("Nombre de retweets");
		entree = lireString();
		entree = boucleVerif(entree);
		if (entree.equalsIgnoreCase("y")) {
			ModelParameter.getInstance()
					.setFieldKept(true, Main.RT_COUNT, true);
		} else
			ModelParameter.getInstance().setFieldKept(true, Main.RT_COUNT,
					false);
		System.out.println("Geolocalisation");
		entree = lireString();
		entree = boucleVerif(entree);
		if (entree.equalsIgnoreCase("y")) {
			ModelParameter.getInstance().setFieldKept(true, Main.LANG, true);
		} else
			ModelParameter.getInstance().setFieldKept(true, Main.LANG, false);
		System.out.println("Date");
		entree = lireString();
		entree = boucleVerif(entree);
		if (entree.equalsIgnoreCase("y")) {
			ModelParameter.getInstance().setFieldKept(true, Main.DATE, true);
		} else
			ModelParameter.getInstance().setFieldKept(true, Main.DATE, false);
		System.out.println("Données sur l'auteur du tweet");
		entree = lireString();
		entree = boucleVerif(entree);
		if (entree.equalsIgnoreCase("y")) {
			ModelParameter.getInstance().setFieldKept(true, Main.AUTHOR, true);
			System.out.println("Nombre de followers de l'auteur");
			entree = lireString();
			entree = boucleVerif(entree);
			if (entree.equalsIgnoreCase("y")) {
				ModelParameter.getInstance().setFieldKept(false,
						Main.FOLLOWERS_CNT, true);
			} else
				ModelParameter.getInstance().setFieldKept(false,
						Main.FOLLOWERS_CNT, false);
			System.out.println("Nombre de follows de l'auteur");
			entree = lireString();
			entree = boucleVerif(entree);
			if (entree.equalsIgnoreCase("y")) {
				ModelParameter.getInstance().setFieldKept(false,
						Main.FOLLOW_CNT, true);
			} else
				ModelParameter.getInstance().setFieldKept(false,
						Main.FOLLOW_CNT, false);
			System.out.println("Pseudo de l'auteur");
			entree = lireString();
			entree = boucleVerif(entree);
			if (entree.equalsIgnoreCase("y")) {
				ModelParameter.getInstance().setFieldKept(false, Main.NAME,
						true);
			} else
				ModelParameter.getInstance().setFieldKept(false, Main.NAME,
						false);
		} else
			ModelParameter.getInstance().setFieldKept(true, Main.AUTHOR, false);

		System.out.println("Image associée au tweet");
		entree = lireString();
		entree = boucleVerif(entree);
		if (entree.equalsIgnoreCase("y")) {
			ModelParameter.getInstance().setFieldKept(true, Main.IMG, true);
		} else
			ModelParameter.getInstance().setFieldKept(true, Main.IMG, false);
		System.out.println("ID du tweet original s'il s'agit d'un RT");
		entree = lireString();
		entree = boucleVerif(entree);
		if (entree.equalsIgnoreCase("y")) {
			ModelParameter.getInstance().setFieldKept(true, Main.RT_ID, true);
		} else
			ModelParameter.getInstance().setFieldKept(true, Main.RT_ID, false);
		System.out.println("Hashtags");
		entree = lireString();
		entree = boucleVerif(entree);
		if (entree.equalsIgnoreCase("y")) {
			ModelParameter.getInstance().setFieldKept(true, Main.HASH, true);
		} else
			ModelParameter.getInstance().setFieldKept(true, Main.HASH, false);
	}

	public static String boucleVerif(String chaine) {
		String entree = chaine;
		while (!entree.equalsIgnoreCase("y") && !entree.equalsIgnoreCase("n")) {
			System.out.println("Try again\n");
			entree = lireString();
		}
		return entree;
	}

	public static String lireString() {// lecture d'une chaine
		String ligne_lue = null;
		try {
			InputStreamReader lecteur = new InputStreamReader(System.in);
			BufferedReader entree = new BufferedReader(lecteur);
			ligne_lue = entree.readLine();
		} catch (IOException err) {
			System.exit(0);
		}
		return ligne_lue;
	}

}
