package App;

import java.awt.Component;

import API.MainAPI;
import Analyze.Analyze;
import Analyze.AnalyzeParameter;
import Data.CurrentAnalyzeInformation;
import Data.Data;
import Data.ModelParameter;
import JDBC.SQLiteJDBC;
import loadAndSave.SaveCSV;
import loadAndSave.SaveDB;

public class Controler {
	private Data modele;
	private CurrentAnalyzeInformation cal;

	public Controler() {
		cal = new CurrentAnalyzeInformation();
		modele = Data.getInstance();
	}

	public static void setBackToTheFuture(Boolean backToTheFuture) {
		ModelParameter.getInstance().setBackToTheFuture(backToTheFuture);
	}

	public static void setTimer(int t) {
		ModelParameter.getInstance().setTimer(t);
	}

	public static void setNB_TWEETS(int nb) {
		ModelParameter.getInstance().setNB_TWEETS(nb);
	}

	public static void setFieldKept(boolean isTweet, int field, boolean isKept) {
		ModelParameter.getInstance().setFieldKept(isTweet, field, isKept);
	}

	public static void setFilterField(boolean isTweet, int field, String filtre) {
		ModelParameter.getInstance().setFilterField(isTweet, field, filtre);
	}

	public void extractAndStore() {
		modele = new MainAPI(ModelParameter.getInstance().getBackToTheFuture()).extractData();
		new SQLiteJDBC().store(modele);
	}

	public void Save(String s, boolean bd) {
		if (bd) {
			new SaveDB(s);
		} else {
			new SaveCSV(s);
		}

	}

	public Data getData() {
		return modele;
	}

	public static void setObservedAuthorId(long id) {
		ModelParameter.getInstance().setObservedAuthorId(id);
	}

	public void switchAnalyze(Analyze analyze, Component observer) {
		cal.switchAnalyze(analyze, observer);
	}

	public void setAnalyzeParameter(int index, AnalyzeParameter p) {
		cal.setAnalyzeParameter(index, p);
	}

	public void lauchnAnalyze() {
		cal.launch();
	}

	public CurrentAnalyzeInformation getCAL() {
		return cal;
	}

}
