package App;

import javax.swing.JFrame;

import Data.Data;
import Graphic.MainWindow;

public class Main extends JFrame {

	public static final int ID = 0;
	public static final int GEOLOC = 1;
	public static final int LANG = 2;
	public static final int DATE = 3;
	public static final int TEXT = 4;
	public static final int HASH = 5;
	public static final int IMG = 6;
	public static final int RT_ID = 7;
	public static final int AUTHOR = 8;
	public static final int REPLY_ID = 9;
	public static final int RT_COUNT = 10;
	public static final int FAV_COUNT = 11;

	public static final int NAME = 1;
	public static final int FOLLOWERS_CNT = 3;
	public static final int FOLLOW_CNT = 4;

	// private Data modele ;
	// private Controler controler ;

	public static void main(String[] argc) {
		Data modele = Data.getInstance();
                if(argc[1].equals("-ig")) {
		Controler controler = new Controler();
		MainWindow window = new MainWindow(controler); // Vue
                }
                else if(argc[1].equals("-console")) {
                    MainConsole.lancerConsole();
                }
	}

}
