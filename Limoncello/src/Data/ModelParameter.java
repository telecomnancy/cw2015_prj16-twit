package Data;

import java.util.Observable;

import Analyze.Analyze;

public final class ModelParameter extends Observable {
	private Boolean arrayOfParametersTweet[];
	private Boolean arrayOfParametersAuthor[];

	private String arrayOfParametersTweetString[];
	private String arrayOfParametersAuthorString[];

	private Boolean visibleConsole = false;
	private long observedAuthorId;
	private boolean backToTheFuture;
	private int timer;
	private int NB_TWEETS;

	private Analyze currentAnalyze;

	public boolean getBackToTheFuture() {
		return backToTheFuture;
	}

	public void setBackToTheFuture(boolean b) {
		this.backToTheFuture = b;
	}

	public int getNB_TWEETS() {
		return NB_TWEETS;
	}

	public void setNB_TWEETS(int NB_TWEETS) {
		this.NB_TWEETS = NB_TWEETS;
	}

	public int getTimer() {
		return timer;
	}

	public void setTimer(int timer) {
		this.timer = timer;
	}

	public long getObservedAuthorId() {
		return observedAuthorId;
	}

	public void setObservedAuthorId(long observedAuthorId) {
		this.observedAuthorId = observedAuthorId;
		setChanged();
		notifyObservers();
	}

	private static volatile ModelParameter parameter;

	private ModelParameter() {
		arrayOfParametersTweet = new Boolean[12];
		arrayOfParametersTweetString = new String[12];
		this.arrayOfParametersTweet[0] = true;
		this.arrayOfParametersTweetString[0] = "";
		for (int i = 1; i < 12; i++) {
			this.arrayOfParametersTweet[i] = true;
			this.arrayOfParametersTweetString[i] = "";
		}
		arrayOfParametersTweet[6] = false;

		arrayOfParametersAuthor = new Boolean[5];
		arrayOfParametersAuthorString = new String[5];
		this.arrayOfParametersAuthor[0] = true;
		for (int i = 1; i < 5; i++) {
			this.arrayOfParametersAuthor[i] = true;
			this.arrayOfParametersAuthorString[i] = "";
		}

		backToTheFuture = true;
		timer = 1;
		NB_TWEETS = 100;

	}

	public final static ModelParameter getInstance() {
		if (ModelParameter.parameter == null) {
			synchronized (ModelParameter.class) {
				if (ModelParameter.parameter == null)
					ModelParameter.parameter = new ModelParameter();
			}
		}
		return ModelParameter.parameter;
	}

	public void setVisibleConsole(Boolean visible) {
		this.visibleConsole = visible;
	}

	public Boolean getVisibleConsole() {
		return visibleConsole;
	}

	public void setFieldKept(boolean isTweet, int field, boolean isKept) {
		if (isTweet) {
			arrayOfParametersTweet[field] = isKept;
		} else {
			arrayOfParametersAuthor[field] = isKept;
		}
	}

	public void setFilterField(boolean isTweet, int field, String filtre) {
		if (isTweet) {
			arrayOfParametersTweetString[field] = filtre;
		} else {
			arrayOfParametersAuthorString[field] = filtre;
		}
	}

	public boolean getFieldKept(boolean isTweet, int field) {

		if (isTweet)
			return arrayOfParametersTweet[field];
		else
			return arrayOfParametersAuthor[field];
	}

	public String getFilterField(boolean isTweet, int field) {
		if (isTweet)
			return arrayOfParametersTweetString[field];
		else
			return arrayOfParametersAuthorString[field];
	}

}
