package Data;

import java.sql.Date;

import twitter4j.GeoLocation;
import App.Main;

public class Tweet {

	private long id;
	private GeoLocation geo;
	private String lang;
	private Date date;
	private String body;
	private String hashtag[];
	private String mediasPath[];
	private Author RTauthor;
	private Author author;
	private int retweetCount;
	private int favoriteCount;
	private long inReplyTo;

	public Tweet(long id) {
		this.id = id;
		this.geo = null;
		this.lang = null;
		this.date = null;
		this.body = null;
		this.hashtag = null;
		this.mediasPath = null;
		this.author = null;
		this.RTauthor = null;
		this.retweetCount = 0;
		this.favoriteCount = 0;
		this.inReplyTo = -1;

	}

	public Object getInt(int indexOfField) {
		switch (indexOfField) {
		case (0):
			return id;
		case (1):
			return geo;
		case (2):
			return lang;
		case (3):
			return date;
		case (4):
			return body;
		case (5):
			return hashtag;
		case (6):
			return mediasPath;
		case (7):
			return RTauthor;
		case (8):
			return author;
		case (9):
			return inReplyTo;
		case (10):
			return retweetCount;
		case (11):
			return favoriteCount;
		default:
			return null;
		}
	}

	public long getInReplyTo() {
		return inReplyTo;
	}

	public void setInReplyTo(long inReplyTo) {
		this.inReplyTo = inReplyTo;
	}

	public int getFavoriteCount() {
		return favoriteCount;
	}

	public void setFavoriteCount(int favoriteCount) {
		this.favoriteCount = favoriteCount;
	}

	public int getRetweetCount() {
		return retweetCount;
	}

	public void setRetweetCount(int retweetCount) {
		this.retweetCount = retweetCount;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public GeoLocation getGeo() {
		return geo;
	}

	public void setGeo(GeoLocation geo) {
		this.geo = geo;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String[] getHashtag() {
		return hashtag;
	}

	public void setHashtag(String hashtag[]) {
		this.hashtag = hashtag;
	}

	public String[] getMediasPath() {
		return mediasPath;
	}

	public void setMediasPath(String mediasPath[]) {
		this.mediasPath = mediasPath;
	}

	public Author getRTauthor() {
		return RTauthor;
	}

	public void setRTauthor(Author RTauthor) {
		this.RTauthor = RTauthor;
	}

	public String toString() {
		String s = "--------------------------------------------------\n";
		s = s + "Tweet:" + id + "\n";
		if (inReplyTo != -1)
			s = s + "In reply to tweet : " + inReplyTo + "\n";
		if (author != null)
			s = s + author.toString() + "\n";
		if (RTauthor != null)
			s = s + RTauthor.toString() + "\n";

		s = "\n" + s + body + "\n";
		/*
		 * for (String h : hashtag) { s = s + "#" + h; }
		 * 
		 * if (hashtag.length > 0) s = s + "\n";
		 */
		s = s + "date:" + date + " place:" + geo + "\n";
		if (ModelParameter.getInstance().getFieldKept(true, Main.RT_COUNT))
			s = s + "ReTweet count=" + retweetCount;
		if (ModelParameter.getInstance().getFieldKept(true, Main.FAV_COUNT))
			s = s + " Favorite count=" + favoriteCount;
		s = s + "\n" + "\n";
		return s;

	}
}
