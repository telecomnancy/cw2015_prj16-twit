package Data;

import java.awt.Component;
import java.util.Observable;

import javax.swing.JPanel;

import Analyze.Analyze;
import Analyze.AnalyzeParameter;
import Analyze.HistogramAnalyze;
import Analyze.Results;
import GraphicAnalyze.AbstractAnalyzePanel;

public class CurrentAnalyzeInformation extends Observable {
	private Analyze analyze;
	private Component resultDisplay;

	public CurrentAnalyzeInformation() {
		analyze = new HistogramAnalyze();
		resultDisplay = new JPanel();
	}

	public void switchAnalyze(Analyze analyze, Component observer) {
		this.analyze = analyze;
		try {
			addObserver((AbstractAnalyzePanel) observer);
		} catch (Exception exception) {

		}
	}

	public void setAnalyzeParameter(int index, AnalyzeParameter p) {
		this.analyze.setParameter(index, p);
	}

	public void launch() {
		Results results = analyze.execute();
		resultDisplay = results.display();
		setChanged();
		notifyObservers();
	}

	public Component getResultDisplay() {
		return resultDisplay;
	}

}
