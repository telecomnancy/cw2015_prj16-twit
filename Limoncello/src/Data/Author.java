package Data;

import java.util.List;

import API.Collect;
import App.Main;

public class Author {
	protected long id;
	protected String name;
	protected int nbFollowers;
	protected int nbFollowed;
	private String language;
	private List<Tweet> timeline;

	public Author(long id) {
		this.id = id;
		this.name = null;
		this.nbFollowers = 0;
		this.nbFollowed = 0;
		this.language = "";
		this.timeline = null;
	}

	public Object getInt(int indexOfField) {
		switch (indexOfField) {
		case (0):
			return id;
		case (1):
			return name;
		case (2):
			return language;
		case (3):
			return nbFollowers;
		case (4):
			return nbFollowed;
		default:
			return null;

		}
	}

	public void setTimeline() {
		this.timeline = new Collect().getTimeline(this.id);
	}

	public List<Tweet> getTimeline() {
		System.out.println(timeline);
		return timeline;
	}

	public void setNbFollowed(int nbFollowed) {
		this.nbFollowed = nbFollowed;
	}

	public int getNbFollowed() {
		return nbFollowed;
	}

	public void setNbFollowers(int nbFollowers) {
		this.nbFollowers = nbFollowers;
	}

	public int getNbFollowers() {
		return nbFollowers;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getLanguage() {
		return language;
	}

	public String toString() {
		String s = "Author :" + id + " " + name + "(" + language + ")" + "\n";
		if (ModelParameter.getInstance().getFieldKept(true, Main.FOLLOWERS_CNT))
			s = s + nbFollowers + " followers   ";
		if (ModelParameter.getInstance().getFieldKept(true, Main.FOLLOW_CNT))
			s = s + nbFollowed + " friends followed";
		return s;
	}
}
