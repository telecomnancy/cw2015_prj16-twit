package Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class Data extends Observable {
	private List<Tweet> Tweetlist;
	private List<Author> Authorlist;

	private static volatile Data instance;

	private Data() {
		super();
		Tweetlist = new ArrayList<Tweet>();
		Authorlist = new ArrayList<Author>();

	}

	public final static Data getInstance() {
		if (Data.instance == null) {
			synchronized (ModelParameter.class) {
				if (Data.instance == null)
					Data.instance = new Data();
			}
		}
		return Data.instance;
	}

	public void addTweet(Tweet tweet) {
		Tweetlist.add(tweet);
		setChanged();
		notifyObservers();
	}

	public Tweet getLastTweet() {
		return Tweetlist.get(Tweetlist.size() - 1);
	}

	public void addAuthor(Author author) {
		Authorlist.add(author);

	}

	public List<Tweet> getTweets() {
		return Tweetlist;
	}

	public List<Author> getAuthors() {
		return Authorlist;
	}

	public void setAuthors(List<Author> La) {
		this.Authorlist = La;
	}

	public void setTweets(List<Tweet> Lt) {
		this.Tweetlist = Lt;
	}

	public List<Tweet> getAuthorTimeline(long authorId) {
		for (Author a : Authorlist) {

			if (a.getId() == authorId) {
				a.setTimeline();
				return a.getTimeline();
			}
		}
		return null;
	}
}
