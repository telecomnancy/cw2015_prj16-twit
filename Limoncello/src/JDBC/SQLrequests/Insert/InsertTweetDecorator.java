package JDBC.SQLrequests.Insert;

import Data.Tweet;

public abstract class InsertTweetDecorator extends InsertRequest {

	protected InsertRequest InsertRequest;// Request on which we add sql
											// statements
	protected Tweet Inserttweet;

	public abstract String getSql(Tweet tweet); // Force objects to implements
												// their insert method

	public InsertTweetDecorator(InsertRequest Request, Tweet tweet) {
		InsertRequest = Request;
		Inserttweet = tweet;
	}

}
