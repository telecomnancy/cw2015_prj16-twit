package JDBC.SQLrequests.Insert;
import Data.*;

public class AuthorDecoratorTweet extends InsertTweetDecorator {
	
	public AuthorDecoratorTweet(InsertRequest Request, Tweet tweet)
    {
    	super(Request, tweet);
    }
    
    // We take the previous request and add the author request to it.
    public String getSql(Tweet tweet)
    {	
    	return InsertRequest.getSql(tweet)+" '"+tweet.getAuthor().getId()+"',";
    
    }
	        
}

