package JDBC.SQLrequests.Insert;

import Data.Author;

public class FollowerCntDecoratorAuthor extends InsertAuthorDecorator {

	public FollowerCntDecoratorAuthor(InsertRequest Request, Author author) {
		super(Request, author);
	}

	// We take the previous request and add the new request to it.
	public String getSql(Author author) {
		return InsertRequest.getSql(author) + " '" + author.getNbFollowers()
				+ "',";

	}

}
