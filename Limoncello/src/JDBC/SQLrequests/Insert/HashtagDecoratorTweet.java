package JDBC.SQLrequests.Insert;

import Data.Tweet;

public class HashtagDecoratorTweet extends InsertTweetDecorator {

	public HashtagDecoratorTweet(InsertRequest Request, Tweet tweet) {
		super(Request, tweet);
	}

	// We take the previous request and add the new request to it.
	public String getSql(Tweet tweet) {
		if (tweet.getHashtag() != null) {
			String[] hash = tweet.getHashtag();
			String previous = InsertRequest.getSql(tweet);
			int i = 0;
			String temp = "";
			for (i = 0; i < hash.length; i++) {
				temp = temp + "INSERT INTO HASHTAGS VALUES ('" + tweet.getId()
						+ "', '" + hash[i] + "');";
			}
			return previous + temp;
		} else
			return "";
	}
}