package JDBC.SQLrequests.Insert;

import Data.Author;
import Data.Tweet;

public class InsertRequest {

	private String sql; // sql request

	public InsertRequest() {
	}

	public InsertRequest(boolean isTweet) {
		if (isTweet) {
			setSql("INSERT INTO TWEETS VALUES ( ");
		} else {
			setSql("INSERT INTO AUTHORS VALUES ( ");
		}
	}

	public String getSql(Tweet tweet) {
		return sql;
	}

	public String getSql(Author author) {
		return sql;
	}

	protected void setSql(String sql) {
		this.sql = sql;
	}

}