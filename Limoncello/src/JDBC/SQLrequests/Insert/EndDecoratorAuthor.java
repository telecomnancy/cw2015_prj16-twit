package JDBC.SQLrequests.Insert;

import Data.Author;

public class EndDecoratorAuthor extends InsertAuthorDecorator {
	public EndDecoratorAuthor(InsertRequest Request, Author author) {
		super(Request, author);
	}

	// We take the previous request and add the author request to it.
	public String getSql(Author author) {
		String temp = InsertRequest.getSql(author);
		String str = temp.substring(0, temp.length() - 1);
		return str + ");";

	}
}
