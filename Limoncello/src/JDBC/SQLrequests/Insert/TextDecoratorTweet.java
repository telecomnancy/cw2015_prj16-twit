package JDBC.SQLrequests.Insert;
import Data.Tweet;

public class TextDecoratorTweet extends InsertTweetDecorator {
	public TextDecoratorTweet(InsertRequest Request, Tweet tweet)
    {
    	super(Request, tweet);
    }
    
    // We take the previous request and add the author request to it.
    public String getSql(Tweet tweet)
    {	
    	return InsertRequest.getSql(tweet)+" '"+tweet.getBody()+"',";
    
    }
}
