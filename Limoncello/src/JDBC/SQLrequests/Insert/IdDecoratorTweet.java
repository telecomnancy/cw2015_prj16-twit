package JDBC.SQLrequests.Insert;

import Data.Tweet;

public class IdDecoratorTweet extends InsertTweetDecorator {

	public IdDecoratorTweet(InsertRequest Request, Tweet tweet) {
		super(Request, tweet);
	}

	// We take the previous request and add the new request to it.
	public String getSql(Tweet tweet) {
		return InsertRequest.getSql(tweet) + " '" + tweet.getId() + "' ,";

	}

}
