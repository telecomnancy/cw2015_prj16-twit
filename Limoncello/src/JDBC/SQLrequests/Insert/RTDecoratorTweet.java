package JDBC.SQLrequests.Insert;

import Data.Tweet;

public class RTDecoratorTweet extends InsertTweetDecorator {
	public RTDecoratorTweet(InsertRequest Request, Tweet tweet) {
		super(Request, tweet);
	}

	// We take the previous request and add the author request to it.
	public String getSql(Tweet tweet) {
		if (tweet.getRTauthor() != null) {
			return InsertRequest.getSql(tweet) + " '"
					+ tweet.getRTauthor().getId() + "',";
		} else
			return InsertRequest.getSql(tweet) + " 'NULL',";

	}
}
