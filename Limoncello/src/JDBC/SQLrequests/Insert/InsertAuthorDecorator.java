package JDBC.SQLrequests.Insert;

import Data.Author;

public abstract class InsertAuthorDecorator extends InsertRequest {

	protected InsertRequest InsertRequest;// Request on which we add sql
											// statements
	protected Author Insertauthor;

	public abstract String getSql(Author author); // Force objects to implements
													// their insert method

	public InsertAuthorDecorator(InsertRequest Request, Author author) {
		InsertRequest = Request;
		Insertauthor = author;
	}

}
