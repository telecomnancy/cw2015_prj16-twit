package JDBC.SQLrequests.Create;

public class AuthorDecorator extends CreateDecorator {
	// The constructor takes the previous request in parameters.
	public AuthorDecorator(CreateRequest Request) {
		super(Request);
	}

	// We take the previous request and add the author request to it.
	public String getSql() {
		return CreateRequest.getSql() + " AUTHOR_ID INT NOT NULL,";
	}

}
