package JDBC.SQLrequests.Create;

public class AuthorNameDecorator extends CreateDecorator {
	// The constructor takes the previous request in parameters.
	public AuthorNameDecorator(CreateRequest Request) {
		super(Request);
	}

	// We take the previous request and add the author request to it.
	public String getSql() {
		return CreateRequest.getSql() + " AUTHOR_NAME CHAR(30) NOT NULL,";
	}

}
