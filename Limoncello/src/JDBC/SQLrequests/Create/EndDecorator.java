package JDBC.SQLrequests.Create;

public class EndDecorator extends CreateDecorator {
	// The constructor takes the previous request in parameters.
	public EndDecorator(CreateRequest Request) {
		super(Request);
	}

	// We take the previous request and add the author request to it.
	public String getSql() {
		String temp = CreateRequest.getSql();
		String str = temp.substring(0, temp.length() - 1);
		return str + ");";
	}

}