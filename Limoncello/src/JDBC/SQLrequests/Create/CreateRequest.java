package JDBC.SQLrequests.Create;

public class CreateRequest {
	private String sql;

	public CreateRequest() {
	}

	public CreateRequest(boolean isTweet) {
		if (isTweet) {
			setSql("CREATE TABLE TWEETS ( ");
		} else {
			setSql("CREATE TABLE AUTHORS ( ");
		}
	}

	public String getSql() {
		return sql;
	}

	protected void setSql(String sql) {
		this.sql = sql;
	}

}