package JDBC.SQLrequests.Create;

public abstract class CreateDecorator extends CreateRequest {
	public CreateDecorator(CreateRequest Request) {
		CreateRequest = Request;
	}

	protected CreateRequest CreateRequest;// Request on which we add sql
											// statements

	public abstract String getSql(); // Force objects to implements their create
										// method
}