package JDBC.SQLrequests.Create;

public class FollowCntDecorator extends CreateDecorator {
	// The constructor takes the previous request in parameters.
	public FollowCntDecorator(CreateRequest Request) {
		super(Request);
	}

	// We take the previous request and add the author request to it.
	public String getSql() {
		return CreateRequest.getSql() + " FOLLOW_CNT INT,";
	}

}