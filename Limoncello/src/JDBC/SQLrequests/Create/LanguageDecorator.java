package JDBC.SQLrequests.Create;

public class LanguageDecorator extends CreateDecorator {
	// The constructor takes the previous request in parameters.
	public LanguageDecorator(CreateRequest Request) {
		super(Request);
	}

	// We take the previous request and add the author request to it.
	public String getSql() {
		return CreateRequest.getSql() + "LANG CHAR(20),";
	}

}
