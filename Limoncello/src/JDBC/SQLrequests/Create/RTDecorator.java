package JDBC.SQLrequests.Create;

public class RTDecorator extends CreateDecorator {
	// The constructor takes the previous request in parameters.
	public RTDecorator(CreateRequest Request) {
		super(Request);
	}

	// We take the previous request and add the author request to it.
	public String getSql() {
		return CreateRequest.getSql() + " RT_ID INT,";
	}

}
