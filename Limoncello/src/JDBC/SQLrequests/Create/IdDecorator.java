package JDBC.SQLrequests.Create;

public class IdDecorator extends CreateDecorator {
	// The constructor takes the previous request in parameters.
	public IdDecorator(CreateRequest Request) {
		super(Request);
	}

	// We take the previous request and add the author request to it.
	public String getSql() {
		return CreateRequest.getSql() + " ID INT PRIMARY KEY NOT NULL,";
	}

}
