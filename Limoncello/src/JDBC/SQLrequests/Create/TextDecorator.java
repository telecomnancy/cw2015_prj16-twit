package JDBC.SQLrequests.Create;

public class TextDecorator extends CreateDecorator {
	// The constructor takes the previous request in parameters.
	public TextDecorator(CreateRequest Request) {
		super(Request);
	}

	// We take the previous request and add the author request to it.
	public String getSql() { // Syntax : space NAME coma (+no space)
		return CreateRequest.getSql() + " TEXT CHAR(150) NOT NULL,";
	}

}
