package JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import App.Main;
import Data.Author;
import Data.Data;
import Data.ModelParameter;
import Data.Tweet;
import JDBC.SQLrequests.Create.AuthorDecorator;
import JDBC.SQLrequests.Create.AuthorNameDecorator;
import JDBC.SQLrequests.Create.CreateRequest;
import JDBC.SQLrequests.Create.DateDecorator;
import JDBC.SQLrequests.Create.EndDecorator;
import JDBC.SQLrequests.Create.FollowCntDecorator;
import JDBC.SQLrequests.Create.FollowerCntDecorator;
import JDBC.SQLrequests.Create.GeolocDecorator;
import JDBC.SQLrequests.Create.IdDecorator;
import JDBC.SQLrequests.Create.LanguageDecorator;
import JDBC.SQLrequests.Create.RTDecorator;
import JDBC.SQLrequests.Create.TextDecorator;
import JDBC.SQLrequests.Insert.AuthorDecoratorTweet;
import JDBC.SQLrequests.Insert.DateDecoratorTweet;
import JDBC.SQLrequests.Insert.EndDecoratorAuthor;
import JDBC.SQLrequests.Insert.EndDecoratorTweet;
import JDBC.SQLrequests.Insert.FollowCntDecoratorAuthor;
import JDBC.SQLrequests.Insert.FollowerCntDecoratorAuthor;
import JDBC.SQLrequests.Insert.GeolocDecoratorTweet;
import JDBC.SQLrequests.Insert.HashtagDecoratorTweet;
import JDBC.SQLrequests.Insert.IdDecoratorAuthor;
import JDBC.SQLrequests.Insert.IdDecoratorTweet;
import JDBC.SQLrequests.Insert.ImageDecoratorTweet;
import JDBC.SQLrequests.Insert.InsertRequest;
import JDBC.SQLrequests.Insert.LanguageDecoratorAuthor;
import JDBC.SQLrequests.Insert.LanguageDecoratorTweet;
import JDBC.SQLrequests.Insert.NameDecoratorAuthor;
import JDBC.SQLrequests.Insert.RTDecoratorTweet;
import JDBC.SQLrequests.Insert.TextDecoratorTweet;

public class SQLiteJDBC {
	private Connection c = null;
	Statement stmt = null;
	public String[] tables = { "TWEETS", "AUTHORS", "HASHTAGS", "IMAGES" };

	public SQLiteJDBC() {
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:Tweets.db");
			c.setAutoCommit(false);
			System.out.println("Opened database successfully");
			stmt = c.createStatement();
			int i = 0;
			String sql;
			for (i = 0; i < 4; i++) {
				try {
					sql = "DROP TABLE " + tables[i] + ";";
					stmt.executeUpdate(sql);
				} catch (SQLException e) {

				}
			}
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}

	public void createListTable(int table) {
		// 0 : table hashtag, 1 : IMG (media entity), 2 : followers, 3 : follows
		String sqlStatement[] = new String[4];
		sqlStatement[0] = "CREATE TABLE HASHTAGS (TWEET_ID INT NOT NULL, HASH CHAR(30) NOT NULL)";
		sqlStatement[1] = "CREATE TABLE IMAGES (TWEET_ID INT  NOT NULL, PATH CHAR(50) NOT NULL)";
		try {
			String sql = sqlStatement[table];
			stmt.executeUpdate(sql);
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}

	public void createBigTable(Data data, boolean isTweet) {
		try {
			String sql = createColumns(data, isTweet);
			stmt.executeUpdate(sql);
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}

	public String createColumns(Data data, boolean isTweet) {
		// create DB columns in function of parameters tab using the decorators
		if (isTweet) { // creation of Tweet table
			CreateRequest r1 = new CreateRequest(true);

			if (ModelParameter.getInstance().getFieldKept(true, Main.ID)) { // id
				r1 = new IdDecorator(r1);
			}
			if (ModelParameter.getInstance().getFieldKept(true, Main.GEOLOC)) { // geolocation
				r1 = new GeolocDecorator(r1);
			}
			if (ModelParameter.getInstance().getFieldKept(true, Main.LANG)) {
				r1 = new LanguageDecorator(r1);
			}
			if (ModelParameter.getInstance().getFieldKept(true, Main.DATE)) {
				r1 = new DateDecorator(r1);
			}
			if (ModelParameter.getInstance().getFieldKept(true, Main.TEXT)) {
				r1 = new TextDecorator(r1);
			}
			if (ModelParameter.getInstance().getFieldKept(true, Main.HASH)) {
				createListTable(0); // Hash table
			}
			if (ModelParameter.getInstance().getFieldKept(true, Main.IMG)) {
				createListTable(1); // Image path table
			}
			if (ModelParameter.getInstance().getFieldKept(true, Main.RT_ID)) {
				r1 = new RTDecorator(r1);
			}
			if (ModelParameter.getInstance().getFieldKept(true, Main.AUTHOR)) {
				r1 = new AuthorDecorator(r1);
			}

			// TO DO : RT COUNT, FAVORITE COUNT, IN REPLY ID
			r1 = new EndDecorator(r1);
			String sql = r1.getSql();
			return sql;
		} else { // creation of Author table
			CreateRequest r2 = new CreateRequest(false);

			if (ModelParameter.getInstance().getFieldKept(false, Main.ID)) { // id
				r2 = new AuthorDecorator(r2);
			}

			if (ModelParameter.getInstance().getFieldKept(false, Main.NAME)) { // name
				r2 = new AuthorNameDecorator(r2);
			}
			if (ModelParameter.getInstance().getFieldKept(false, Main.FOLLOWERS_CNT)) {
				r2 = new FollowerCntDecorator(r2);
			}
			if (ModelParameter.getInstance().getFieldKept(false, Main.FOLLOW_CNT)) {
				r2 = new FollowCntDecorator(r2);
			}
			if (ModelParameter.getInstance().getFieldKept(false, Main.LANG)) {
				r2 = new LanguageDecorator(r2);
			}
			r2 = new EndDecorator(r2);
			String sql = r2.getSql();
			return sql;
		}
	}

	public void insertTweet(Tweet tweet) {
		try {
			String sql = insertTweetAttributes(tweet);
			stmt.executeUpdate(sql);
			c.commit();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}

	public void insertAuthor(Author author) {
		try {
			String sql = insertAuthorAttributes(author);
			stmt.executeUpdate(sql);
			c.commit();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

	}

	public String insertTweetAttributes(Tweet tweet) {
		// creating the begging of the request ("CREATE TABLE ...")
		InsertRequest r2 = new InsertRequest(true);

		if (ModelParameter.getInstance().getFieldKept(true, Main.ID)) { // id
			r2 = new IdDecoratorTweet(r2, tweet);
		}
		if (ModelParameter.getInstance().getFieldKept(true, Main.GEOLOC)) { // geoloc
			r2 = new GeolocDecoratorTweet(r2, tweet);
		}
		if (ModelParameter.getInstance().getFieldKept(true, Main.LANG)) { // language
			r2 = new LanguageDecoratorTweet(r2, tweet);
		}
		if (ModelParameter.getInstance().getFieldKept(true, Main.DATE)) { // date
			r2 = new DateDecoratorTweet(r2, tweet);
		}
		if (ModelParameter.getInstance().getFieldKept(true, Main.TEXT)) { // text
			r2 = new TextDecoratorTweet(r2, tweet);
		}
		if (ModelParameter.getInstance().getFieldKept(true, Main.RT_ID)) { // retweet
			r2 = new RTDecoratorTweet(r2, tweet);
		}
		if (ModelParameter.getInstance().getFieldKept(true, Main.AUTHOR)) { // author
			r2 = new AuthorDecoratorTweet(r2, tweet);
		}
		// TO DO : RT COUNT, FAVORITE COUNT, IN REPLY ID

		r2 = new EndDecoratorTweet(r2, tweet);
		String sql = r2.getSql(tweet);

		if (ModelParameter.getInstance().getFieldKept(true, Main.HASH) && tweet.getHashtag() != null) { // hashtag
			r2 = new HashtagDecoratorTweet(r2, tweet);
		}

		if (ModelParameter.getInstance().getFieldKept(true, Main.IMG) && tweet.getMediasPath() != null) { // image
			r2 = new ImageDecoratorTweet(r2, tweet);
		}
		sql = r2.getSql(tweet);
		return sql;

	}

	public String insertAuthorAttributes(Author author) {
		// creating the begging of the request ("CREATE TABLE ...")
		InsertRequest r2 = new InsertRequest(false);
		// tests on parameter booleans
		if (ModelParameter.getInstance().getFieldKept(false, Main.ID)) { // id
			r2 = new IdDecoratorAuthor(r2, author);
		}
		if (ModelParameter.getInstance().getFieldKept(false, Main.NAME)) {
			r2 = new NameDecoratorAuthor(r2, author);
		}
		if (ModelParameter.getInstance().getFieldKept(false, Main.FOLLOWERS_CNT)) {
			r2 = new FollowerCntDecoratorAuthor(r2, author);
		}
		if (ModelParameter.getInstance().getFieldKept(false, Main.FOLLOW_CNT)) {
			r2 = new FollowCntDecoratorAuthor(r2, author);
		}
		if (ModelParameter.getInstance().getFieldKept(false, Main.LANG)) {
			r2 = new LanguageDecoratorAuthor(r2, author);
		}
		r2 = new EndDecoratorAuthor(r2, author);

		String sql = r2.getSql(author);
		return sql;

	}

	public void store(Data data) {
		createBigTable(data, true); // tweet Table
		createBigTable(data, false); // Author Table
		for (Tweet tweet : data.getTweets()) {
			insertTweet(tweet);
		}
		for (Author author : data.getAuthors()) {
			insertAuthor(author);
		}
		try {

			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("Tables stored");
	}
}