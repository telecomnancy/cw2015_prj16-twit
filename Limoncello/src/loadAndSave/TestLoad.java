package loadAndSave;

import API.MainAPI;
import App.Controler;
import App.Main;
import Data.Author;
import Data.Data;
import Data.ModelParameter;
import Data.Tweet;
import JDBC.SQLiteJDBC;
import java.util.ArrayList;
import java.util.List;

public class TestLoad {

    public static void main(String[] argc){
        List<Tweet> tweets = new ArrayList<Tweet>();
        List<Author> authors = new ArrayList<Author>();
        
        Data modele = Data.getInstance();
	Controler controler = new Controler();
        
        ModelParameter parameter = ModelParameter.getInstance();
		for (int i = 0; i < 12; i++) {
			ModelParameter.getInstance().setFieldKept(true, i, true);
		}
		for (int i = 0; i < 5; i++) {
			ModelParameter.getInstance().setFieldKept(false, i, true);
		}
                ModelParameter.getInstance().setFieldKept(true, Main.GEOLOC, false);
		new SQLiteJDBC().store(new MainAPI(false).extractData());
                tweets = modele.getTweets();
                authors = modele.getAuthors();
                System.out.println("On sauvegarde notre collecte dans TestDB.db après avoir stocké les listes de tweets et d'auteurs dans deux listes tweets et authors");
                new SaveDB("TestDB.db");
                System.out.println("On charge TestDB.db");
                new Load("TestDB.db");
                Boolean marche = true;
                System.out.println("On compare tweet par tweet et auteur par auteur pour vérifier qu'ils sont tous identiques");
                for(int i = 0; i < tweets.size(); i++) {
                    if(!tweets.get(i).equals(modele.getTweets().get(i)) || !authors.get(i).equals(modele.getAuthors().get(i))) {
                        marche = false;
                    }
                }
                if(marche) {
                    System.out.println("Les tweets et auteurs récupérés dans la base de données qu'on vient de charger correspondent aux tweets/auteurs de la collecte initiale.");
                }else System.out.println("Les tweets/auteurs récupérés sont différents de ceux de la collecte initiale");
                
    }
}
