package loadAndSave;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import Data.Author;
import Data.Data;
import Data.Tweet;

public class Load {

	public Load(String name) {

		try {

			Statement stmt = null;
			Data FinalData;
			List<String> TableNames = new ArrayList<String>();
			String[][][] Datas; // In the order, dimensions represents tables
			// names, columns names and columns contents
			int NumberOfTweet;
			int NumberOfAuthor = 0;
			int NumberOfHash = 0;

			Class.forName("org.sqlite.JDBC");
			Connection conn = DriverManager
					.getConnection("jdbc:sqlite:Save/SaveDB/" + name);
			Statement request = conn.createStatement();
			// First step: we know that there is a table called TWEETS, and we
			// need to have the number of tweets and authors, and hashtag
			// aparently

			String sqlQueryNumbrOfTweet = "SELECT COUNT(*) FROM TWEETS;";
			ResultSet numbrOfTweet = request.executeQuery(sqlQueryNumbrOfTweet);
			NumberOfTweet = Integer.parseInt(numbrOfTweet.getObject(1)
					.toString());
			try {
				String sqlQueryNumbrOfAuthor = "SELECT COUNT(*) FROM AUTHOR;";
				ResultSet numbrOfAuthor = request
						.executeQuery(sqlQueryNumbrOfAuthor);
				NumberOfAuthor = Integer.parseInt(numbrOfTweet.getObject(1)
						.toString());
			} catch (SQLException e) {

			}

			try {
				String sqlQueryNumbrOfHash = "SELECT COUNT(*) FROM HASHTAGS;";
				ResultSet numbrOfHash = request
						.executeQuery(sqlQueryNumbrOfHash);
				NumberOfHash = Integer.parseInt(numbrOfHash.getObject(1)
						.toString());
			} catch (SQLException e) {

			}
			// Second step: Get all table names

			String sqlQueryTableNames = "SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;";
			try {
				ResultSet tableNames = request.executeQuery(sqlQueryTableNames);
				String[] arr = null;
				while (tableNames.next()) {
					TableNames.add(tableNames.getString(1));
				}
			} catch (SQLException e) {
				System.err
						.println("Load: erreur dans la capture du nom des tables");
			}

			// Third step: Get all Columns names

			Datas = new String[TableNames.size()][12][Math.max(
					NumberOfTweet + 1, NumberOfHash + 1)]; // NumberOfTweet
			// +
			// 1
			// because the name of a column is save in [x][y][0], with x a table
			// and y a column

			for (int i = 0; i < TableNames.size(); i++) {
				String sqlQueryColumnNames = "SELECT * FROM "
						+ TableNames.get(i) + ";";
				ResultSet columnNames = request
						.executeQuery(sqlQueryColumnNames);
				ResultSetMetaData columnNamesData = columnNames.getMetaData();
				for (int j = 1; j <= columnNamesData.getColumnCount(); j++) {
					Datas[i][j][0] = columnNamesData.getColumnName(j);

				}
			}

			// Fourth step: Get all the datas for all the columns, for all the
			// table

			for (int i = 1; i < TableNames.size(); i++) {
				for (int j = 0; j < 12; j++) {
					// Construct the sql request for a column
					if (Datas[i][j][0] != null) {
						String sqlQueryColumnContent = "SELECT "
								+ Datas[i][j][0] + " FROM " + TableNames.get(i)
								+ ";";

						ResultSet columnContent = request
								.executeQuery(sqlQueryColumnContent);
						// Add all the datas in there third dimension column
						int k = 1;
						while (columnContent.next()) {
							Datas[i][j][k] = columnContent.getString(1);
							k++;

						}

					}
				}
			}

			// Five Step: Contruct the Dataclass to throw

			List<Tweet> ListTweet = new ArrayList<>();
			List<Author> ListAuthor = new ArrayList<>();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");

			for (int i = 0; i < TableNames.size(); i++) {
				if (TableNames.get(i) == "AUTHORS") {
					for (int j = 0; j < 12; j++) {
						if (Datas[i][j][0] == "AUTHOR_ID") {
							for (int k = 0; k < NumberOfTweet; k++) {
								ListAuthor.add(new Author(Long
										.parseLong(Datas[i][j][k])));
							}
						}
						if (Datas[i][j][0] == "AUTHOR_NAME") {
							for (int k = 0; k < NumberOfTweet; k++) {
								ListAuthor.get(k).setName(Datas[i][j][k]);
							}
						}
						if (Datas[i][j][0] == "FOLLOWER_CNT") {
							for (int k = 0; k < NumberOfTweet; k++) {
								ListAuthor.get(k).setNbFollowers(
										Integer.parseInt(Datas[i][j][k]));
							}
						}
						if (Datas[i][j][0] == "FOLLOW_CNT") {
							for (int k = 0; k < NumberOfTweet; k++) {
								ListAuthor.get(k).setNbFollowers(
										Integer.parseInt(Datas[i][j][k]));
							}
						}
						if (Datas[i][j][0] == "LANG") {
							for (int k = 0; k < NumberOfTweet; k++) {
								ListAuthor.get(k).setLanguage(Datas[i][j][k]);
							}
						}
					}
				}
				if (TableNames.get(i) == "TWEETS") {

					for (int j = 0; j < 12; j++) {
						if (Datas[i][j][0] == "ID") {
							for (int k = 0; k < NumberOfTweet; k++) {
								ListTweet.add(new Tweet(Long
										.parseLong(Datas[i][j][k])));
							}
						}
						if (Datas[i][j][0] == "COUNTRY") {
							for (int k = 0; k < NumberOfTweet; k++) {
								ListTweet.get(k).setGeo(null);// No way to make
								// GeoLocation
								// type with
								// these
								// unusable
								// string !
							}
						}

						if (Datas[i][j][0] == "LANGUE") {
							for (int k = 0; k < NumberOfTweet; k++) {
								ListTweet.get(k).setLang(Datas[i][j][k]);
							}
						}

						if (Datas[i][j][0] == "DATE") {
							for (int k = 0; k < NumberOfTweet; k++) {
								try {
									Date date = formatter.parse(Datas[i][j][k]);
								} catch (ParseException e) {
									System.out.println("Load: uncastable date");
								}
								ListTweet.get(k).setLang(Datas[i][j][k]);
							}
						}

						if (Datas[i][j][0] == "TEXT") {
							for (int k = 0; k < NumberOfTweet; k++) {
								ListTweet.get(k).setBody(Datas[i][j][k]);
							}
						}

						if (Datas[i][j][0] == "RT_ID") {
							for (int k = 0; k < NumberOfTweet; k++) {
								Author a = new Author(
										Long.parseLong(Datas[i][j][k]));
								ListTweet.get(k).setRTauthor(a);
							}
						}

						if (Datas[i][j][0] == "AUTHOR_ID") {
							for (int k = 0; k < NumberOfTweet; k++) {
								for (int l = 0; k < NumberOfAuthor; l++) {
									if (ListAuthor.get(l).getId() == Long
											.parseLong(Datas[i][j][k])) {
										ListTweet.get(k).setAuthor(
												ListAuthor.get(l));

									}
								}
							}
						}
					}

				}
			}

		} catch (SQLException | ClassNotFoundException e) {
			System.err.println("Load: Impossible de se connecté à " + name
					+ ".db\n");

		}
	}
}