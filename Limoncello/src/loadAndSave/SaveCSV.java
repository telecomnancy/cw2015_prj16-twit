package loadAndSave;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class SaveCSV {
	public SaveCSV(String output) {
		try {
			new File("Save").mkdir();
			new File("Save/SaveCSV").mkdir();
			Class.forName("org.sqlite.JDBC");
			String dbURL = "jdbc:sqlite:Tweets.db";
			Connection conn = DriverManager.getConnection(dbURL);
			if (conn != null) {
				String tablename;
				System.out.println("Connected to the database");
				DatabaseMetaData md = conn.getMetaData();
				ResultSet rs = md.getTables(null, null, "%", null);
				while (rs.next()) {
					tablename = rs.getString(3);
					exportData(conn, output + "." + tablename, tablename);
				}
				conn.close();
			}
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("La base de données a été exportée en CSV.");
	}

	public void exportData(Connection conn, String filename, String tablename) {
		Statement stmt;
		String query;
		try {
			stmt = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);

			// For comma separated file
			BufferedWriter output = null;
			File file = new File("Save/SaveCSV/" + filename + ".txt");
			output = new BufferedWriter(new FileWriter(file));
			ArrayList<String[]> result = new ArrayList<String[]>();
			ResultSet rs = stmt
					.executeQuery("SELECT * FROM " + tablename + ";");
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnCount = rsmd.getColumnCount();
			// The column count starts from 1
			for (int i = 1; i <= columnCount; i++) {
				String name = rsmd.getColumnName(i);
				if (i == columnCount) {
					output.write(name);
				} else
					output.write(name + ",");
			}
			output.write("\n");
			while (rs.next()) // As long as there's a new line
			{
				String[] row = new String[columnCount]; // new row
				for (int i = 0; i < columnCount; i++) // for each column
				{
					if (i == columnCount - 1) {
						row[i] = rs.getString(i + 1);
					} else {
						row[i] = rs.getString(i + 1) + ","; // store the string
															// with
					}
					output.write(row[i]);
				}
				output.write("\n");
				result.add(row);
			}
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
			stmt = null;

		}

	}
}
