package loadAndSave;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class SaveDB {

	public SaveDB(String name) {
		FileChannel in = null; // canal d'entrée
		FileChannel out = null; // canal de sortie
		{
			new File("Save").mkdir();
			new File("Save/SaveDB").mkdir();

			try {
				// Init
				in = new FileInputStream("Tweets.db").getChannel();
				out = new FileOutputStream("Save/SaveDB/" + name + ".db")
						.getChannel();

				// Copie depuis le in vers le out
				in.transferTo(0, in.size(), out);
			} catch (Exception e) {
				e.printStackTrace(); // n'importe quelle exception
			} finally { // finalement on ferme
				if (in != null) {
					try {
						in.close();
					} catch (IOException e) {
					}
				}

				if (out != null) {
					try {
						out.close();
					} catch (IOException e) {
					}
				}
			}
		}
	}

}
